# Lagalt - project overview
This application is about finding projects that you're intrested in, when the user has found a project that they would like to be a 
part of they can create an account and join the project. On every indivudual project there is a message board. where all logged in users can post comments
to easier collaborate with each other. 

### Frontend - [Lagalt website](https://frontend-lagalt-group-seven.herokuapp.com)
The frontend is created with React, we also used the css framework tailwind to quicker and easier build our react components. 
The Website contains multiple pages, so we are also using react router so that our website would be a SPA(Single page application).

### Backend
On the backend we are using C# with ASP.NET Core Web API.

### Database
For the connection with the database we are using Entity framwork. 
We were using SSMS for early stages of development and the deployed version is using Azure Data Studio. We use a code first approach.
![Database Diagram](Database-diagram.png?raw=true "Diagram")
 
### Authentication
We are using keycloak to authenticate our users. We have our own deployed version of keycloak. 

### Tools
- Visual Studio Code
- Visual Studio 2022
- .NET 6
- ASP.NET Core Web API 
- Microsoft SQL Server Management Studio.
- Azure Data Studio

## Deployment
### Frontend
Our react frontend is hosted as a git repo on Heroku. 
### Backend
We are using Azure to host our backend as a docker image.
### Keycloak
The keycloak instance is deployed using a docker image. the docker image is containerized on Heroku.

# Startup guide
If you would like to run the project locally on your on machine. These steps will help you achive just that.

### Step 1 `Git clone`. 
Firstly you need to clone the entire project to your machine using `git clone https://gitlab.com/Endu22/lagalt-case-group-7.git`

## Step 2 `Frontend`.
CD into the front end folder and write `npm install` in your terminal.
After node packages are install you can now write `npm start` this will boot up a local server. Go to `http://localhost:3000/`

## Step 3 `Backend`.
Go into your back-end folder and double-click the `Lagalt-backend.sln` file.
In Visual studio now run the application, Swagger will open and the rest api is live on `https://localhost:7299/swagger`. 

## Step 4 `Database`.
You will need to connect your own SSMS in the `Program.cs` file

``` c#
builder.Services.AddDbContext<LagaltDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString(<---Your connectionString here --->)));
```
## Step 5 `Authentication server`
You need to have your own authentication server. A good choice would be to use [jboss/keycloak](https://hub.docker.com/r/jboss/keycloak) docker image on dockerhub.
and the host it in a container on heroku 


# Developers
[Sofia Vulgari](https://www.linkedin.com/in/sofia-vulgari/)
[Oliver Rimmi](https://www.linkedin.com/in/oliver-rimmi-490972236/)
[Daniel Bengtsson](https://www.linkedin.com/in/daniel-bengtsson-0baa881a3/)

