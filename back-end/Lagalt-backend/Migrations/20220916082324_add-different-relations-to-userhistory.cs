﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lagalt_backend.Migrations
{
    public partial class adddifferentrelationstouserhistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectUserHistory_UsersHistory_UserHistoryId",
                table: "ProjectUserHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_UsersHistory_Users_UserId",
                table: "UsersHistory");

            migrationBuilder.DropIndex(
                name: "IX_UsersHistory_UserId",
                table: "UsersHistory");

            migrationBuilder.RenameColumn(
                name: "RecordCounter",
                table: "UsersHistory",
                newName: "ProjectId");

            migrationBuilder.RenameColumn(
                name: "UserHistoryId",
                table: "ProjectUserHistory",
                newName: "UserHistoriesUserHistoryId");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectUserHistory_UserHistoryId",
                table: "ProjectUserHistory",
                newName: "IX_ProjectUserHistory_UserHistoriesUserHistoryId");

            migrationBuilder.CreateTable(
                name: "UserUserHistory",
                columns: table => new
                {
                    UserHistoryId = table.Column<int>(type: "int", nullable: false),
                    UsersUserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserUserHistory", x => new { x.UserHistoryId, x.UsersUserId });
                    table.ForeignKey(
                        name: "FK_UserUserHistory_Users_UsersUserId",
                        column: x => x.UsersUserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserUserHistory_UsersHistory_UserHistoryId",
                        column: x => x.UserHistoryId,
                        principalTable: "UsersHistory",
                        principalColumn: "UserHistoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserUserHistory_UsersUserId",
                table: "UserUserHistory",
                column: "UsersUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectUserHistory_UsersHistory_UserHistoriesUserHistoryId",
                table: "ProjectUserHistory",
                column: "UserHistoriesUserHistoryId",
                principalTable: "UsersHistory",
                principalColumn: "UserHistoryId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectUserHistory_UsersHistory_UserHistoriesUserHistoryId",
                table: "ProjectUserHistory");

            migrationBuilder.DropTable(
                name: "UserUserHistory");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "UsersHistory",
                newName: "RecordCounter");

            migrationBuilder.RenameColumn(
                name: "UserHistoriesUserHistoryId",
                table: "ProjectUserHistory",
                newName: "UserHistoryId");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectUserHistory_UserHistoriesUserHistoryId",
                table: "ProjectUserHistory",
                newName: "IX_ProjectUserHistory_UserHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_UsersHistory_UserId",
                table: "UsersHistory",
                column: "UserId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectUserHistory_UsersHistory_UserHistoryId",
                table: "ProjectUserHistory",
                column: "UserHistoryId",
                principalTable: "UsersHistory",
                principalColumn: "UserHistoryId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UsersHistory_Users_UserId",
                table: "UsersHistory",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
