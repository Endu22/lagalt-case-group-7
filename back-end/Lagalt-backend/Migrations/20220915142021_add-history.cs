﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lagalt_backend.Migrations
{
    public partial class addhistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Fields_UsersHistory_UserHistoryId",
                table: "Fields");

            migrationBuilder.DropIndex(
                name: "IX_Fields_UserHistoryId",
                table: "Fields");

            migrationBuilder.DropColumn(
                name: "UserHistoryId",
                table: "Fields");

            migrationBuilder.CreateTable(
                name: "ProjectUserHistory",
                columns: table => new
                {
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    UserHistoryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectUserHistory", x => new { x.ProjectId, x.UserHistoryId });
                    table.ForeignKey(
                        name: "FK_ProjectUserHistory_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "ProjectId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjectUserHistory_UsersHistory_UserHistoryId",
                        column: x => x.UserHistoryId,
                        principalTable: "UsersHistory",
                        principalColumn: "UserHistoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectUserHistory_UserHistoryId",
                table: "ProjectUserHistory",
                column: "UserHistoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectUserHistory");

            migrationBuilder.AddColumn<int>(
                name: "UserHistoryId",
                table: "Fields",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fields_UserHistoryId",
                table: "Fields",
                column: "UserHistoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Fields_UsersHistory_UserHistoryId",
                table: "Fields",
                column: "UserHistoryId",
                principalTable: "UsersHistory",
                principalColumn: "UserHistoryId");
        }
    }
}
