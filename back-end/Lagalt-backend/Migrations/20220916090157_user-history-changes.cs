﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lagalt_backend.Migrations
{
    public partial class userhistorychanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectUserHistory_Projects_ProjectId",
                table: "ProjectUserHistory");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "ProjectUserHistory",
                newName: "ProjectsProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectUserHistory_Projects_ProjectsProjectId",
                table: "ProjectUserHistory",
                column: "ProjectsProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectUserHistory_Projects_ProjectsProjectId",
                table: "ProjectUserHistory");

            migrationBuilder.RenameColumn(
                name: "ProjectsProjectId",
                table: "ProjectUserHistory",
                newName: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectUserHistory_Projects_ProjectId",
                table: "ProjectUserHistory",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
