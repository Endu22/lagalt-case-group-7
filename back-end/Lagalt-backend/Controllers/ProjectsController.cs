﻿using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Admin;
using Lagalt_backend.Model.DTOs.Message;
using Lagalt_backend.Model.DTOs.Project;
using Lagalt_backend.Model.DTOs.User;
using Lagalt_backend.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        /// <summary>
        /// Get all projects
        /// </summary>
        /// <returns>All the projects</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectReadDTO>>> GetAllProjects() => await _projectService.GetAllProjects();

        [HttpGet("sorted/{userId}")]
        public async Task<ActionResult<IEnumerable<ProjectReadDTO>>> GetAllProjectsByUserId(int userId)
        {
            return await _projectService.GetProjectsByUserId(userId);
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectReadDTO>> GetProjectById(int id) => await _projectService.GetProjectById(id);

        /// <summary>
        /// Get all admins in a project by id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns>All admins of the project</returns>
        [HttpGet("{projectId}/admins")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllAdminsInProject(int projectId)
        {
            return await _projectService.GetAllAdmins(projectId);
        }

        /// <summary>
        /// Get admin by id in a project by id
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="adminId"></param>
        /// <returns>The specified admin of the specified project</returns>
        [HttpGet("{projectId}/admin/{adminId}")]
        public async Task<ActionResult<UserReadDTO>> GetAdminByIdInProject(int projectId, int adminId)
        {
            return await _projectService.GetAdminByIdInProject(projectId, adminId);
        }

        /// <summary>
        /// Get all projects based on search string
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns>All the projects based the search string</returns>
        [HttpGet("search/{searchString}")]
        public async Task<ActionResult<IEnumerable<ProjectReadDTO>>> GetAllProjectsBasedOnSearchString(string searchString)
        {
            return await _projectService.SearchFor(searchString);
        }

        /// <summary>
        /// Get all messages in a project by id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns>All the messages of the specified project</returns>
        [HttpGet("messages/{projectId}")]
        public async Task<ActionResult<IEnumerable<MessageReadDTO>>> GetMessagesInProject(int projectId)
        {
            if (await _projectService.GetProjectById(projectId) == null)
                return BadRequest();

            return await _projectService.GetAllMessagesInProject(projectId);
        }

        /// <summary>
        /// Get all members of a project by id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns>All members of the specified project</returns>
        [HttpGet("{projectId}/members")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllMembersOfProject(int projectId)
        {
            if (await _projectService.GetProjectById(projectId) == null)
                return BadRequest();

            return await _projectService.GetMembersInProject(projectId);
        }

        /// <summary>
        /// Update a project by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="projectUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProject(int id, ProjectUpdateDTO projectUpdateDto)
        {
            if (id != projectUpdateDto.ProjectId)
                return BadRequest();

            if (!await _projectService.ProjectExists(id))
                return NotFound();

            await _projectService.UpdateProject(id, projectUpdateDto);

            return NoContent();
        }

        /// <summary>
        /// Create a project
        /// </summary>
        /// <param name="projectCreateDto"></param>
        /// <returns>The created project</returns>
        [HttpPost]
        public async Task<ActionResult<ProjectReadDTO>> CreateProject(ProjectCreateDTO projectCreateDto)
        {
            if (!ProjectEntityExists())
                return Problem("Entity is null");

            var project = await _projectService.CreateProject(projectCreateDto);

            List<User> admins = new();

            // Create new admin
            AdminCreateDTO adminCreateDTO = new() { ProjectId = project.ProjectId, UserId = project.CreatorId };

            await _projectService.GrantUserInProjectAdminRights(adminCreateDTO);
            return CreatedAtAction("GetProjectById", new { id = project.ProjectId }, project);
        }

        /// <summary>
        /// Create an admin on a project by id.
        /// Make a specific user, admin of the specified project 
        /// </summary>
        /// <param name="adminId"></param>
        /// <param name="userId"></param>
        /// <param name="projectId"></param>
        /// <param name="adminCreateDTO"></param>
        /// <returns>The admin of the project</returns>
        [HttpPost("{adminId}/grant/{userId}/admin/{projectId}")]
        public async Task<ActionResult<AdminReadDTO>> GrantUserInProjectAdminRights(int adminId, int userId, int projectId, AdminCreateDTO adminCreateDTO)
        {
            if (adminCreateDTO.UserId != userId && adminCreateDTO.ProjectId != projectId)
                return BadRequest();

            if (!await _projectService.ProjectExists(projectId))
                return BadRequest();

            if (!await _projectService.AdminExistsInProject(adminId, projectId))
                return BadRequest();

            if (!await _projectService.UserExistsInProject(userId, projectId))
                return BadRequest();

            return await _projectService.GrantUserInProjectAdminRights(adminCreateDTO);
        }

        /// <summary>
        /// Update members in a projects by id
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="members"></param>
        /// <returns></returns>
        [HttpPut("members/{projectId}")]
        public async Task<ActionResult> UpdateMembersInProject(int projectId, List<int> members)
        {
            var updated = await _projectService.UpdateMembersProject(projectId, members);
            if (updated) return NoContent();

            return BadRequest();
        }

        /// <summary>
        /// Create a message
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="messageCreateDTO"></param>
        /// <returns></returns>
        [HttpPost("{projectId}")]
        public async Task<ActionResult> AddMessage(int projectId, MessageCreateDTO messageCreateDTO)
        {
            if (await _projectService.GetProjectById(projectId) == null)
                return BadRequest();

            if (!await _projectService.UserExistsInProject(messageCreateDTO.MessageCreatorId, projectId))
                return BadRequest();

            var created = await _projectService.AddMessage(projectId, messageCreateDTO);

            if (!created)
                return BadRequest();
            return NoContent();
        }

        /// <summary>
        /// Delete a project
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProject(int id)
        {
            if (!ProjectEntityExists())
                return Problem("Entity does not exist.");

            var project = await _projectService.DeleteProject(id);

            if (project == null)
                return NotFound();

            return NoContent();
        }

        /// <summary>
        /// Delete an admin
        /// </summary>
        /// <param name="adminId"></param>
        /// <returns></returns>
        [HttpDelete("remove/admin/{adminId}")]
        public async Task<IActionResult> RemoveAdminRightsForAdmin(int adminId)
        {
            if (!AdminEntityExists())
                return Problem("Entity does not exist.");

            bool deletedRights = await _projectService.RemoveAdminRights(adminId);

            if (deletedRights)
                return NoContent();

            return BadRequest();
        }

        /// <summary>
        /// Check if project entity exists
        /// </summary>
        /// <returns>A bool</returns>
        private bool ProjectEntityExists()
        {
            return _projectService.ProjectEntityExists();
        }

        /// <summary>
        /// Check if admin entity exists
        /// </summary>
        /// <returns>A bool</returns>
        private bool AdminEntityExists()
        {
            return _projectService.AdminEntityExists();
        }
    }
}
