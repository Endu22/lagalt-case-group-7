﻿using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Field;
using Lagalt_backend.Model.DTOs.Skill;
using Lagalt_backend.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FieldsController : ControllerBase
    {
        private readonly IFieldService _context;

        public FieldsController(IFieldService context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all fields
        /// </summary>
        /// <returns>All the fields</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FieldReadDTO>>> GetFields()
        {
            return await _context.GetAllFields();
        }

        /// <summary>
        /// Update skills of a field
        /// </summary>
        /// <param name="id"></param>
        /// <param name="skills"></param>
        /// <returns></returns>
        [HttpPut("skills/{id}")]
        public async Task<IActionResult> PutSkillsOnField(int id, List<int> skills)
        {
            try
            {
                await _context.UpdateSkillsOnField(id, skills);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid skill.");
            }

            return NoContent();
        }

        /// <summary>
        /// Get a field by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Ok result</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Field>> GetField(int id)
        {
            return Ok(await _context.FindById(id));
        }

        /// <summary>
        /// Get all skills in a field by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>All the skills in the specified field</returns>
        [HttpGet("{id}/skills")]
        public async Task<IEnumerable<SkillReadDTO>> GetSkillsInField(int id)
        {
            return await _context.GetAllSkillsInField(id);
        }

        /// <summary>
        /// Update a field
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fieldUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateField(int id, FieldUpdateDTO fieldUpdateDto)
        {
            if (id != fieldUpdateDto.FieldId)
                return BadRequest();

            if (!await _context.FieldExists(id))
                return NotFound();

            await _context.UpdateField(id, fieldUpdateDto);

            return NoContent();
        }

        /// <summary>
        /// Create a new field
        /// </summary>
        /// <param name="fieldDTO"></param>
        /// <returns>The created field</returns>
        [HttpPost]
        public async Task<ActionResult<Field>> PostField(FieldCreateDTO fieldDTO)
        {
            var domainField = await _context.Create(fieldDTO);

            return CreatedAtAction(
                "GetField",
                new { id = domainField.FieldId },
                fieldDTO
                );
        }

        /// <summary>
        /// Delete a field
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Ok result</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteField(int id)
        {
            bool didDelete = await _context.remove(id);

            if (!didDelete) return NotFound();

            return Ok();
        }

        /// <summary>
        /// Check if character exists
        /// </summary>
        /// <returns>A bool</returns>
        private bool FieldEntityExists()
        {
            return _context.FieldEntityExists();
        }
    }
}
