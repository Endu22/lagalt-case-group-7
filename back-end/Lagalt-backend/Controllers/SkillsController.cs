﻿using Lagalt_backend.Model.DTOs.Skill;
using Lagalt_backend.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillsController : ControllerBase
    {
        private readonly ISkillService _skillService;

        public SkillsController(ISkillService skillService)
        {
            _skillService = skillService;
        }

        /// <summary>
        /// Get all skills
        /// </summary>
        /// <returns>All the skills</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SkillReadDTO>>> GetAllSkills() => await _skillService.GetAllSkills();

        /// <summary>
        /// Get a skill by id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The specified skill</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<SkillReadDTO>> GetSkillById(int id) => await _skillService.GetSkillById(id);

        /// <summary>
        /// Update a skill by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="skillUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateSkill(int id, SkillUpdateDTO skillUpdateDto)
        {
            if (id != skillUpdateDto.SkillId)
                return BadRequest();

            if (!await _skillService.SkillExists(id))
                return NotFound();

            await _skillService.UpdateSkill(id, skillUpdateDto);

            return NoContent();
        }

        /// <summary>
        /// Create a skill
        /// </summary>
        /// <param name="skillCreateDto"></param>
        /// <returns>The created skill</returns>
        [HttpPost]
        public async Task<ActionResult<SkillCreateDTO>> CreateSkill(SkillCreateDTO skillCreateDto)
        {
            if (!SkillEntityExists())
                return Problem("Entity is null");

            var skill = await _skillService.CreateSkill(skillCreateDto);

            return CreatedAtAction("Create Skill", new { id = skill.SkillId }, skill);
        }

        /// <summary>
        /// Delete a skill by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSkill(int id)
        {
            if (!SkillEntityExists())
                return Problem("Entity is null.");

            var skill = await _skillService.DeleteSkill(id);

            if (skill == null)
                return NotFound();

            return NoContent();
        }

        /// <summary>
        /// Check if skill exists
        /// </summary>
        /// <returns>A bool</returns>
        private bool SkillEntityExists()
        {
            return _skillService.SkillEntityExists();
        }
    }
}
