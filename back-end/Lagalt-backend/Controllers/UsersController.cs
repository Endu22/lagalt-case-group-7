﻿using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.User;
using Lagalt_backend.Model.DTOs.Skill;
using Lagalt_backend.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Lagalt_backend.Model.DTOs.UserHistory;
using Microsoft.AspNetCore.Authorization;

namespace Lagalt_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllUsers() => await _userService.GetAllUsers();

        // GET: api/Users/5
        [HttpGet("{userId}")]
        public async Task<ActionResult<UserReadDTO>> GetUserById(int userId) => await _userService.GetUserById(userId);

        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<User>> CreateUser(UserCreateDTO userCreateDTO)
        {
            if (!_userService.UserEntityExists())
                return Problem("Users Entity does not exist.");

            var userReadDTO = await _userService.CreateUser(userCreateDTO);

            return CreatedAtAction("CreateUser", new { id = userReadDTO.UserId }, userReadDTO);
        }

        [HttpGet("skills/{userId}")]
        public async Task<ActionResult<IEnumerable<SkillReadDTO>>> GetAllSkillsOfUser(int userId)
        {
           if (await _userService.GetUserById(userId) == null)
                    return BadRequest();

                return await _userService.GetAllSkillsInUser(userId);
            
        } 

        [HttpPut("skills/{userId}")]
        [Authorize]
        public async Task<ActionResult> PutSkillsOnUser(int userId, int[] skillsIds)
        {
            if (await _userService.GetUserById(userId) == null)
                return BadRequest();

            var updated = await _userService.UpdateSkillsOfUser(userId, skillsIds);

            if (!updated)
                return BadRequest();

            return NoContent();
        }

        [HttpPut("projects/{userId}")]
        [Authorize]
        public async Task<ActionResult> PutProjectsOnUser(int userId, int[] projectIds)
        {
            if (await _userService.GetUserById(userId) == null)
                return BadRequest();

            var updated = await _userService.UpdateProjectsOfUser(userId, projectIds);

            if (!updated)
                return BadRequest();

            return NoContent();
        }

        [HttpPut("history/{userId}")]
        [Authorize]
        public async Task<ActionResult> UpdateUserHistory(int userId, UserHistoryUpdateDTO userHistoryUpdateDTO)
        {
            if (await _userService.GetUserById(userId) == null)
                return BadRequest();

            if (await _userService.GetProjectById(userHistoryUpdateDTO.ProjectId) == null)
                return BadRequest();

            await _userService.UpdateUserHistory(userId, userHistoryUpdateDTO);

            return NoContent();
        }

        [HttpDelete("{userId}")]
        [Authorize]
        public async Task<ActionResult<UserReadDTO>> DeleteUser(int userId)
        {
            if (!_userService.UserEntityExists())
                return BadRequest();

            var user = await _userService.DeleteUser(userId);

            if (user == null)
                return NotFound();

            return Ok(user);
        }
    }
}
