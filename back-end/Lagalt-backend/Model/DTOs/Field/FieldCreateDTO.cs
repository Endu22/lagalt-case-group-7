﻿namespace Lagalt_backend.Model.DTOs.Field
{
    public class FieldCreateDTO
    {
        public string Name { get; set; }
    }
}
