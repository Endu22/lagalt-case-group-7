﻿namespace Lagalt_backend.Model.DTOs.Field
{
    public class FieldUpdateDTO
    {
        public int FieldId { get; set; }
        public string Name { get; set; }
    }
}
