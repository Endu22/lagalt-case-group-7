﻿namespace Lagalt_backend.Model.DTOs.Field
{
    public class FieldReadDTO
    {
        public string Name { get; set; }
        public ICollection<int> Skills { get; set; }
        public int FieldId { get; set; }
    }
}
