﻿namespace Lagalt_backend.Model.DTOs.Skill
{
    public class SkillCreateDTO
    {
        public string Description { get; set; }
        public string SkillName { get; set; }

    }
}
