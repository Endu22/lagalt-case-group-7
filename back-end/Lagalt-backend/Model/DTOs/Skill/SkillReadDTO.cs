﻿namespace Lagalt_backend.Model.DTOs.Skill
{
    public class SkillReadDTO
    {
        public int SkillId { get; set; }
        public string SkillName { get; set; }
        public string Description { get; set; }
        public ICollection<int> Users { get; set; }
        public ICollection<int> Projects { get; set; }
    }
}
