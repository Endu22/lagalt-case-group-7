﻿namespace Lagalt_backend.Model.DTOs.Message
{
    public class MessageCreateDTO
    {
        public string MessageText { get; set; }
        public int MessageCreatorId { get; set; }
    }
}
