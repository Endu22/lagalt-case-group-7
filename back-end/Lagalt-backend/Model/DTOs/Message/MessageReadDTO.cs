﻿namespace Lagalt_backend.Model.DTOs.Message
{
    public class MessageReadDTO
    {
        public int MessageId { get; set; }
        public string MessageText { get; set; }
        public int MessageCreatorId { get; set; }
    }
}
