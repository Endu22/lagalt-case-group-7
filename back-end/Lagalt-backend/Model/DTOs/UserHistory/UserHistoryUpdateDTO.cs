﻿namespace Lagalt_backend.Model.DTOs.UserHistory
{
    public class UserHistoryUpdateDTO
    {
        public int UserId { get; set; }
        public int ProjectId { get; set; }
    }
}
