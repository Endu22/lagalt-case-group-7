﻿namespace Lagalt_backend.Model.DTOs.Project
{
    public class ProjectReadDTO
    {
        public int ProjectId { get; set; }
        public int CreatorId { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public ICollection<int> Members { get; set; }
        public ICollection<int> Admins { get; set; }
        public int FieldId { get; set; }
    }
}
