﻿namespace Lagalt_backend.Model.DTOs.Project
{
    public class ProjectCreateDTO
    {
        public int CreatorId { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public int FieldId { get; set; }
    }
}
