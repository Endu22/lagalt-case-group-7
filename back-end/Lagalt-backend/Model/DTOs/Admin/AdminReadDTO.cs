﻿namespace Lagalt_backend.Model.DTOs.Admin
{
    public class AdminReadDTO
    {
        public int AdminId { get; set; }
        public int UserId { get; set; }
        public ICollection<int> Admins { get; set; }
        public int ProjectId { get; set; }
        public ICollection<int> Projects { get; set; }
    }
}
