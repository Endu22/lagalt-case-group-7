﻿namespace Lagalt_backend.Model.DTOs.Admin
{
    public class AdminCreateDTO
    {
        public int UserId { get; set; }
        public int ProjectId { get; set; }
    }
}
