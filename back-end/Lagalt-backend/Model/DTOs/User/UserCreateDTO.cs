﻿namespace Lagalt_backend.Model.DTOs.User
{
    public class UserCreateDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Username { get; set; }
    }
}
