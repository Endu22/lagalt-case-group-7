﻿namespace Lagalt_backend.Model.DTOs.User
{
    public class UserReadDTO
    {
        public int UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string? Username { get; set; }
        public ICollection<int>? Projects { get; set; }
        public ICollection<int>? Skills { get; set; }
    }
}
