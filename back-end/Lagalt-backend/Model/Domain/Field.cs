﻿using System.ComponentModel.DataAnnotations;

namespace Lagalt_backend.Model.Domain
{
    /// <summary>
    /// Model for the Field
    /// </summary>
    public class Field
    {
        public int FieldId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        public ICollection<Project>? Projects { get; set; }
        public ICollection<Skill>? Skills { get; set; }
    }
}
