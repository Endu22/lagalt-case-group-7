﻿using System.ComponentModel.DataAnnotations;

namespace Lagalt_backend.Model.Domain
{
    /// <summary>
    /// Model for the User
    /// </summary>
    public class User
    {
        public int UserId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Firstname { get; set; }
        [MaxLength(50)]
        public string Lastname { get; set; }
        [MaxLength(50)]
        public string Username { get; set; }
        public ICollection<Project>? Projects { get; set; }
        public ICollection<Skill> Skills { get; set; }
        public ICollection<Admin> Admins { get; set; }
        public ICollection<UserHistory>? UserHistory { get; set; }
    }
}
