﻿using System.ComponentModel.DataAnnotations;

namespace Lagalt_backend.Model.Domain
{
    /// <summary>
    /// Model for the Project
    /// </summary>
    public class Project
    {
        public int ProjectId { get; set; }
        public int CreatorId { get; set; }
        public string ProjectName { get; set; }
        [MaxLength(350)]
        public string Description { get; set; }
        public ICollection<User>? Members { get; set; }
        public int? FieldId { get; set; }
        public Field? Field { get; set; }
        public ICollection<Skill>? Skills { get; set; }
        public ICollection<Admin>? Admins { get; set; }
        public ICollection<Message> Messages { get; set; }
        public ICollection<UserHistory>? UserHistories { get; set; }
    }
}
