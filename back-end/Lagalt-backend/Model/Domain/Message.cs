﻿using System.ComponentModel.DataAnnotations;

namespace Lagalt_backend.Model.Domain
{
    /// <summary>
    /// Model for the Message
    /// </summary>
    public class Message
    {
        public int MessageId { get; set; }
        [MaxLength(300)]
        public string MessageText { get; set; }
        public int MessageCreatorId { get; set; }
        public User MessageCreator { get; set; }
    }
}
