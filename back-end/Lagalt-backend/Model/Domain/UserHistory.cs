﻿namespace Lagalt_backend.Model.Domain
{
    /// <summary>
    /// Model for the UserHistory
    /// </summary>
    public class UserHistory
    {
        public int UserHistoryId { get; set; }
        public int UserId { get; set; }
        public ICollection<User> Users { get; set; }
        public int ProjectId { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}
