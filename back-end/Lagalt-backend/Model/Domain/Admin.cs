﻿namespace Lagalt_backend.Model.Domain
{
    /// <summary>
    /// Model for the Admin
    /// </summary>
    public class Admin
    {
        public int AdminId { get; set; }
        //[ForeignKey("Admins")]
        public int UserId { get; set; }
        public ICollection<User> Admins { get; set; }
        //[ForeignKey("Projects")]
        public int ProjectId { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}
