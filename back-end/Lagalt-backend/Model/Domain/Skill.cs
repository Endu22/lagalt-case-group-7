﻿using System.ComponentModel.DataAnnotations;

namespace Lagalt_backend.Model.Domain
{
    /// <summary>
    /// Model for the Skill
    /// </summary>
    public class Skill
    {
        public int SkillId { get; set; }
        [MaxLength(50)]
        public string SkillName { get; set; }
        [MaxLength(300)]
        public string Description { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}
