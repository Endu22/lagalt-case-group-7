﻿using AutoMapper;
using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Field;
using Lagalt_backend.Model.DTOs.Skill;
using Lagalt_backend.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Services
{
    /// <summary>
    /// Field Service
    /// </summary>
    public class FieldService : IFieldService
    {
        private readonly IFieldRepository _FieldRepository;
        private readonly ISkillRepository _skillRepository;
        private readonly IMapper _mapper;

        public FieldService(IFieldRepository fieldRepository, ISkillRepository skillRepository, IMapper mapper)
        {
            _FieldRepository = fieldRepository;
            _skillRepository = skillRepository;
            _mapper = mapper;
        }

        public async Task<Field> Create(FieldCreateDTO field)
        {
            return await _FieldRepository.Create(_mapper.Map<Field>(field));
        }

        public async Task<FieldReadDTO> FindById(int id)
        {
            var field = await _FieldRepository.GetAll(id);

            if (field == null) throw new Exception("field is not found");

            return _mapper.Map<FieldReadDTO>(field);
        }

        public async Task<ActionResult<IEnumerable<FieldReadDTO>>> GetAllFields()
        {
            return _mapper.Map<List<FieldReadDTO>>(await _FieldRepository.GetAll());
        }

        public async Task<IEnumerable<SkillReadDTO>> GetAllSkillsInField(int id)
        {
            return _mapper.Map<List<SkillReadDTO>>(await _FieldRepository.GetAllSkillsInField(id));
        }

        public async Task UpdateSkillsOnField(int id, List<int> skills)
        {
            var field = await _FieldRepository.FieldToUpdateSkills(id);

            List<Skill> skillss = new();
            foreach (int skillId in skills)
            {
                var sk = await _skillRepository.GetSkill(skillId);

                if (sk == null)
                    throw new KeyNotFoundException();

                skillss.Add(sk);
            }

            field.Skills = skillss;

            await _FieldRepository.SaveChanges();
        }

        public async Task<bool> UpdateField(int id, FieldUpdateDTO fieldUpdateDto)
        {
            var field = await _FieldRepository.GetAll(id);

            _mapper.Map(fieldUpdateDto, field);

            return await _FieldRepository.UpdateField(field);
        }

        public async Task<bool> remove(int id)
        {
            return await _FieldRepository.Delete(id);
        }

        public bool FieldEntityExists()
        {
            return _FieldRepository.FieldEntityExists();
        }

        public async Task<bool> FieldExists(int id)
        {
            return await _FieldRepository.FieldExists(id);
        }
    }
}
