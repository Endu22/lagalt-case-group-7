﻿using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Field;
using Lagalt_backend.Model.DTOs.Skill;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Services.Interfaces
{
    /// <summary>
    /// Service Interface for the Field
    /// </summary>
    public interface IFieldService
    {
        public Task<ActionResult<IEnumerable<FieldReadDTO>>> GetAllFields();

        public Task<Field> Create(FieldCreateDTO field);

        public Task<bool> remove(int id);

        public Task<FieldReadDTO> FindById(int id);

        public Task<bool> UpdateField(int id, FieldUpdateDTO fieldUpdateDto);

        public Task UpdateSkillsOnField(int id, List<int> skills);

        public Task<IEnumerable<SkillReadDTO>> GetAllSkillsInField(int id);

        public bool FieldEntityExists();

        public Task<bool> FieldExists(int id);
    }
}
