using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Admin;
using Lagalt_backend.Model.DTOs.Message;
using Lagalt_backend.Model.DTOs.Project;
using Lagalt_backend.Model.DTOs.User;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Services.Interfaces
{
    /// <summary>
    /// Service Interface for the Project
    /// </summary>
    public interface IProjectService
    {
        public Task<ActionResult<IEnumerable<ProjectReadDTO>>> GetAllProjects();
        public Task<ActionResult<IEnumerable<ProjectReadDTO>>> GetProjectsByUserId(int userId);
        public Task<ActionResult<ProjectReadDTO>> GetProjectById(int id);
        public Task<ActionResult<IEnumerable<MessageReadDTO>>> GetAllMessagesInProject(int projectId);
        public Task<ActionResult<IEnumerable<UserReadDTO>>> GetMembersInProject(int projectId);
        public Task<ActionResult<IEnumerable<ProjectReadDTO>>> SearchFor(string key);
        public Task<bool> UpdateProject(int id, ProjectUpdateDTO skillUpdateDto);
        public Task<ActionResult<AdminReadDTO>> GrantUserInProjectAdminRights(AdminCreateDTO adminCreateDTO);
        public Task<bool> AddMessage(int projectId, MessageCreateDTO messageCreateDTO);
        public Task<ProjectReadDTO> CreateProject(ProjectCreateDTO projectCreateDTO);
        public Task<Project> DeleteProject(int id);
        public Task<bool> RemoveAdminRights(int adminId);
        public bool ProjectEntityExists();
        public bool AdminEntityExists();
        public Task<bool> AdminExists(int adminId); // consider removing this.
        public Task<bool> AdminExistsInProject(int adminId, int projectId);
        public Task<bool> UserExistsInProject(int userId, int projectId);
        public Task<bool> ProjectExists(int id);
        public Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllAdmins(int projectId);
        public Task<UserReadDTO> GetAdminByIdInProject(int projectId, int adminId);
        public Task<bool> UpdateMembersProject(int projectId, List<int> members);
    }
}
