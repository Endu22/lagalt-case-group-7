﻿using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Skill;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Services.Interfaces
{
    /// <summary>
    /// Service Interface for the Skill
    /// </summary>
    public interface ISkillService
    {
        public Task<ActionResult<IEnumerable<SkillReadDTO>>> GetAllSkills();
        public Task<ActionResult<SkillReadDTO>> GetSkillById(int id);
        public Task<bool> UpdateSkill(int id, SkillUpdateDTO skillUpdateDto);
        public Task<Skill> CreateSkill(SkillCreateDTO skillCreateDTO);
        public Task<Skill> DeleteSkill(int id);
        public bool SkillEntityExists();
        public Task<bool> SkillExists(int id);
    }
}
