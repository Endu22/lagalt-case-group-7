﻿using Lagalt_backend.Model.DTOs.Project;
using Lagalt_backend.Model.DTOs.Skill;
using Lagalt_backend.Model.DTOs.User;
using Lagalt_backend.Model.DTOs.UserHistory;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Services.Interfaces
{
    /// <summary>
    /// Service Interface for the User
    /// </summary>
    public interface IUserService
    {
        public Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllUsers();
        public Task<ActionResult<UserReadDTO>> GetUserById(int userId);
        public Task<ActionResult<ProjectReadDTO>> GetProjectById(int projectId);
        public Task<ActionResult<IEnumerable<SkillReadDTO>>> GetAllSkillsInUser(int id);
        public Task<UserReadDTO> CreateUser(UserCreateDTO userCreateDTO);
        public Task<bool> UpdateSkillsOfUser(int userId, int[] skillIds);
        public Task<bool> UpdateProjectsOfUser(int userId, int[] projectIds);
        public Task<bool> UpdateUserHistory(int userId, UserHistoryUpdateDTO userHistoryUpdateDTO);
        public Task<UserReadDTO> DeleteUser(int userId);
        public bool UserEntityExists();
    }
}
