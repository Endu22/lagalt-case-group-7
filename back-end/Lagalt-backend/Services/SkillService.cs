﻿using AutoMapper;
using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Skill;
using Lagalt_backend.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Services
{
    /// <summary>
    /// Skill Service
    /// </summary>
    public class SkillService : ISkillService
    {
        private readonly ISkillRepository _skillRepository;
        private readonly IMapper _mapper;

        public SkillService(ISkillRepository skillRepository, IMapper mapper)
        {
            _skillRepository = skillRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult<IEnumerable<SkillReadDTO>>> GetAllSkills()
        {
            return _mapper.Map<List<SkillReadDTO>>(await _skillRepository.GetAll());
        }

        public async Task<ActionResult<SkillReadDTO>> GetSkillById(int id)
        {
            return _mapper.Map<SkillReadDTO>(await _skillRepository.GetSkill(id));
        }

        public async Task<bool> UpdateSkill(int id, SkillUpdateDTO skillUpdateDto)
        {
            var skill = await _skillRepository.GetSkill(id);

            _mapper.Map(skillUpdateDto, skill);

            return await _skillRepository.UpdateSkill(skill);
        }

        public async Task<Skill> CreateSkill(SkillCreateDTO skillCreateDto)
        {
            var skill = _mapper.Map<Skill>(skillCreateDto);

            return await _skillRepository.CreateSkill(skill);
        }

        public async Task<Skill> DeleteSkill(int id)
        {
            return await _skillRepository.DeleteSkill(id);
        }

        public bool SkillEntityExists()
        {
            return _skillRepository.SkillEntityExists();
        }

        public async Task<bool> SkillExists(int id)
        {
            return await _skillRepository.SkillExists(id);
        }
    }
}
