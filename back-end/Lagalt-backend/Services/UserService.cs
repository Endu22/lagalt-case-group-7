﻿using AutoMapper;
using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Project;
using Lagalt_backend.Model.DTOs.Skill;
using Lagalt_backend.Model.DTOs.User;
using Lagalt_backend.Model.DTOs.UserHistory;
using Lagalt_backend.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Services
{
    /// <summary>
    /// User Service
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ISkillRepository _skillRepository;
        private readonly IUserHistoryRepository _userHistoryRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IProjectRepository projectRepository, ISkillRepository skillRepository, IUserHistoryRepository userHistoryRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _projectRepository = projectRepository;
            _skillRepository = skillRepository;
            _userHistoryRepository = userHistoryRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllUsers()
        {
            return _mapper.Map<List<UserReadDTO>>(await _userRepository.GetAll());
        }

        public async Task<ActionResult<UserReadDTO>> GetUserById(int userId)
        {
            return _mapper.Map<UserReadDTO>(await _userRepository.GetUserById(userId));
        }

        public async Task<ActionResult<ProjectReadDTO>> GetProjectById(int projectId)
        {
            return _mapper.Map<ProjectReadDTO>(await _projectRepository.GetProjectById(projectId));
        }

        public async Task<UserReadDTO> CreateUser(UserCreateDTO userCreateDTO)
        {
            var user = _mapper.Map<User>(userCreateDTO);

            return _mapper.Map<UserReadDTO>(await _userRepository.CreateUser(user));
        }

        public async Task<ActionResult<IEnumerable<SkillReadDTO>>> GetAllSkillsInUser(int userId)
        {
            var user = await _userRepository.GetUserWithSkills(userId);

            List<SkillReadDTO> skills = new();
            if (user.Skills != null)
            {
                foreach (var skill in user.Skills)
                {
                    skills.Add(_mapper.Map<SkillReadDTO>(skill));
                }
            }

            return skills;
        }

        public async Task<bool> UpdateSkillsOfUser(int userId, int[] skillIds)
        {
            var userToUpdate = await _userRepository.GetUserById(userId);

            List<Skill> skills = new();
            foreach (var skillId in skillIds)
            {
                Skill domainSkill = await _skillRepository.GetSkill(skillId);

                if (domainSkill == null)
                    throw new KeyNotFoundException();

                skills.Add(domainSkill);
            }

            userToUpdate.Skills = skills;

            var updated = await _userRepository.UpdateUser(userToUpdate);

            if (updated == false)
                return false;

            return true;
        }

        public async Task<bool> UpdateProjectsOfUser(int userId, int[] projectIds)
        {
            var userToUpdate = await _userRepository.GetUserById(userId);

            List<Project> projects = new();
            foreach (var projectId in projectIds)
            {
                Project domianProject = await _projectRepository.GetProjectById(projectId);

                if (domianProject == null)
                    throw new KeyNotFoundException();

                projects.Add(domianProject);
            }

            userToUpdate.Projects = projects;

            var updated = await _userRepository.UpdateUser(userToUpdate);

            if (updated == false)
                return false;

            return true;
        }

        public async Task<bool> UpdateUserHistory(int userId, UserHistoryUpdateDTO userHistoryUpdateDTO)
        {
            var domainUser = await _userRepository.GetUserById(userHistoryUpdateDTO.UserId);
            var domainProject = await _projectRepository.GetProjectById(userHistoryUpdateDTO.ProjectId);

            UserHistory historyToAdd = new();

            if (historyToAdd.Projects == null)
            {
                historyToAdd.Projects = new List<Project>();
                historyToAdd.Projects.Add(domainProject);
            }
            else
            {
                historyToAdd.Projects.Add(domainProject);
            }

            if (historyToAdd.Users == null)
            {
                historyToAdd.Users = new List<User>();
                historyToAdd.Users.Add(domainUser);
            }
            else
            {
                historyToAdd.Users.Add(domainUser);
            }

            historyToAdd.ProjectId = domainProject.ProjectId;
            historyToAdd.UserId = domainUser.UserId;

            return await _userHistoryRepository.CreateUserHistoryOnUser(historyToAdd);
        }

        public async Task<UserReadDTO> DeleteUser(int userId)
        {
            return _mapper.Map<UserReadDTO>(await _userRepository.DeleteUser(userId));
        }

        public bool UserEntityExists()
        {
            return _userRepository.UserEntityExists();
        }
    }
}
