using AutoMapper;
using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Admin;
using Lagalt_backend.Model.DTOs.Message;
using Lagalt_backend.Model.DTOs.Project;
using Lagalt_backend.Model.DTOs.User;
using Lagalt_backend.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Services
{
    /// <summary>
    /// Project Service
    /// </summary>
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IAdminRepository _adminRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public ProjectService(IProjectRepository projectRepository, IAdminRepository adminRepository, IUserRepository userRepository, IMapper mapper)
        {
            _projectRepository = projectRepository;
            _adminRepository = adminRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult<IEnumerable<ProjectReadDTO>>> GetAllProjects()
        {
            return _mapper.Map<List<ProjectReadDTO>>(await _projectRepository.GetAll());
        }

        public async Task<ActionResult<IEnumerable<ProjectReadDTO>>> GetProjectsByUserId(int userId)
        {
            var user = await _userRepository.GetUserById(userId);

            List<Project> fieldOneProjectsInHistory = new List<Project>();
            List<Project> fieldTwoProjectsInHistory = new List<Project>();
            List<Project> fieldThreeProjectsInHistory = new List<Project>();
            List<Project> fieldFourProjectsInHistory = new List<Project>();
            List<Project> fieldFiveProjectsInHistory = new List<Project>();
            List<List<int>> userHistoryIdList = new();

            //Array of lists
            List<Project>[] lists = new List<Project>[5];

            if (user.UserHistory != null)
            {

                userHistoryIdList.Add(user.UserHistory.Select(u => u.ProjectId).ToList());

                foreach (var projectList in userHistoryIdList)
                {
                    foreach (var projectId in projectList)
                    {
                        var project = await _projectRepository.GetProjectById(projectId);

                        if (project.FieldId == 1)
                            fieldOneProjectsInHistory.Add(project);

                        if (project.FieldId == 2)
                            fieldTwoProjectsInHistory.Add(project);

                        if (project.FieldId == 3)
                            fieldThreeProjectsInHistory.Add(project);

                        if (project.FieldId == 4)
                            fieldFourProjectsInHistory.Add(project);
                    }
                }


                lists[0] = fieldOneProjectsInHistory;
                lists[1] = fieldTwoProjectsInHistory;
                lists[2] = fieldThreeProjectsInHistory;
                lists[3] = fieldFourProjectsInHistory;
                lists[4] = fieldFiveProjectsInHistory;

                // Sort depending on length.
                int[] fieldIds = new int[] { 1, 2, 3, 4, 5 }; // int array representing the field id of the lists of projects
                for (int i = 0; i < lists.Length - 1; i++)
                {
                    if (lists[i].Count() < lists[i + 1].Count())
                    {
                        var tmp = lists[i];
                        lists[i] = lists[i + 1];
                        lists[i + 1] = tmp;

                        var tmpTwo = fieldIds[i];
                        fieldIds[i] = fieldIds[i + 1];
                        fieldIds[i + 1] = tmpTwo;
                        i = 0;
                    }
                }

                List<Project> listToReturn = new List<Project>();
                foreach (var fieldId in fieldIds)
                {
                    listToReturn.AddRange(await _projectRepository.GetProjectsByFieldId(fieldId));
                }

                return _mapper.Map<List<ProjectReadDTO>>(listToReturn);
            }
            return _mapper.Map<List<ProjectReadDTO>>(await _projectRepository.GetAll());
        }

        public async Task<ActionResult<ProjectReadDTO>> GetProjectById(int id)
        {
            return _mapper.Map<ProjectReadDTO>(await _projectRepository.GetProjectById(id));
        }

        public async Task<ActionResult<IEnumerable<MessageReadDTO>>> GetAllMessagesInProject(int projectId)
        {
            var project = await _projectRepository.GetProjectWithMessages(projectId);

            List<MessageReadDTO> messages = new();
            if (project.Messages != null)
            {
                foreach (var message in project.Messages)
                {
                    messages.Add(_mapper.Map<MessageReadDTO>(message));
                }
            }

            return messages;
        }

        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetMembersInProject(int projectId)
        {
            return _mapper.Map<List<UserReadDTO>>(await _projectRepository.GetMembersInProject(projectId));
        }

        public async Task<ActionResult<IEnumerable<ProjectReadDTO>>> SearchFor(string key)
        {
            return _mapper.Map<List<ProjectReadDTO>>(await _projectRepository.GetProjectsByKey(key));
        }

        public async Task<bool> UpdateProject(int id, ProjectUpdateDTO projectUpdateDto)
        {
            var project = await _projectRepository.GetProjectById(id);

            _mapper.Map(projectUpdateDto, project);

            return await _projectRepository.UpdateProject(project);
        }

        public async Task<ActionResult<AdminReadDTO>> GrantUserInProjectAdminRights(AdminCreateDTO adminCreateDTO)
        {
            var domainUser = await _userRepository.GetUserById(adminCreateDTO.UserId);
            var domainProject = await _projectRepository.GetProjectById(adminCreateDTO.ProjectId);

            Admin adminToAdd = new();

            if (adminToAdd.Projects == null)
            {
                adminToAdd.Projects = new List<Project>();
                adminToAdd.Projects.Add(domainProject);
            }
            else
            {
                adminToAdd.Projects.Add(domainProject);
            }

            if (adminToAdd.Admins == null)
            {
                adminToAdd.Admins = new List<User>();
                adminToAdd.Admins.Add(domainUser);
            }
            else
            {
                adminToAdd.Admins.Add(domainUser);
            }

            adminToAdd.ProjectId = domainProject.ProjectId;
            adminToAdd.UserId = domainUser.UserId;

            return _mapper.Map<AdminReadDTO>(await _adminRepository.CreateAdminInProject(adminToAdd));
        }

        public async Task<bool> AddMessage(int projectId, MessageCreateDTO messageCreateDTO)
        {
            var message = new Message();

            message.MessageCreator = await _userRepository.GetUserById(messageCreateDTO.MessageCreatorId);
            message.MessageText = messageCreateDTO.MessageText;

            return await _projectRepository.AddMessage(projectId, message);
        }

        public async Task<ProjectReadDTO> CreateProject(ProjectCreateDTO projectCreateDto)
        {
            var project = _mapper.Map<Project>(projectCreateDto);

            List<User> members = new();

            members.Add(await _userRepository.GetUserById(project.CreatorId));

            project.Members = members;

            return _mapper.Map<ProjectReadDTO>(await _projectRepository.CreateProject(project));
        }

        public async Task<Project> DeleteProject(int id)
        {
            return await _projectRepository.DeleteProject(id);
        }

        public async Task<bool> RemoveAdminRights(int adminId)
        {
            return await _adminRepository.RemoveAdminRights(adminId);
        }

        public bool ProjectEntityExists()
        {
            return _projectRepository.ProjectEntityExists();
        }

        public bool AdminEntityExists()
        {
            return _adminRepository.AdminEntityExists();
        }

        public async Task<bool> AdminExists(int adminId)
        {
            return await _adminRepository.AdminExists(adminId);
        }

        public async Task<bool> AdminExistsInProject(int adminId, int projectId)
        {
            return await _adminRepository.AdminExistsInProject(adminId, projectId);
        }

        public async Task<bool> ProjectExists(int id)
        {
            return await _projectRepository.ProjectExists(id);
        }

        public async Task<bool> UserExistsInProject(int userId, int projectId)
        {
            return await _projectRepository.UserExistsInProject(userId, projectId);
        }

        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllAdmins(int projectId)
        {
            return _mapper.Map<List<UserReadDTO>>(await _adminRepository.GetAdminsInProject(projectId));
        }

        public async Task<UserReadDTO> GetAdminByIdInProject(int projectId, int adminId)
        {
            return _mapper.Map<UserReadDTO>(await _adminRepository.GetAdminInProject(projectId, adminId));
        }

        public async Task<bool> UpdateMembersProject(int projectId, List<int> members)
        {
            List<User> domainMembers = new();
            foreach (var member in members)
            {
                domainMembers.Add(await _userRepository.GetUserById(member));
            }

            return await _projectRepository.UpdateMembersInProject(projectId, domainMembers);
        }
    }
}
