﻿using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace Lagalt_backend.Database.Repositories
{
    /// <summary>
    /// Field Repository
    /// </summary>
    public class FieldRepository : IFieldRepository
    {
        private readonly LagaltDbContext _context;

        public FieldRepository(LagaltDbContext context)
        {
            _context = context;
        }

        public async Task<Field> Create(Field field)
        {
            if (field != null) await _context.Fields.AddAsync(field);

            if (_context.SaveChangesAsync().Result == 0) throw new Exception("No CHanges were mage in the database");

            return field is not null ? field : throw new NullReferenceException();
        }

        public async Task<bool> Delete(int id)
        {
            Field field = await GetAll(id);

            if (field == null) return false;

            _context.Fields.Remove(field);

            if (_context.SaveChangesAsync().Result == 0) throw new Exception("No hcanges were mage in the database");

            return true;
        }

        public async Task<IEnumerable<Field>> GetAll()
        {
            return await _context.Fields.Include(f => f.Skills).ToListAsync();
        }

        public async Task<Field> GetAll(int id)
        {
            Field? field = await _context.Fields.Include(f => f.Skills).SingleOrDefaultAsync(f => f.FieldId == id);

            return field is not null ? field : throw new NullReferenceException();
        }

        public async Task<bool> UpdateField(Field field)
        {
            _context.Entry(field).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return true;
        }

        public async Task<Field> FieldToUpdateSkills(int id)
        {
            Field? field = await _context.Fields.Include(c => c.Skills)
                .Where(c => c.FieldId == id)
                .FirstAsync();

            return field is not null ? field : throw new NullReferenceException();
        }

        public async Task<List<Skill>> GetAllSkillsInField(int id)
        {
            //get Skill id
            Field field = await _context.Fields.Where(a => a.FieldId == id).Include(a => a.Skills).FirstOrDefaultAsync();

            List<Skill> skills = new List<Skill>();

            foreach (var skill in field.Skills)
            {
                skills.Add(skill);
            }

            return skills;
        }

        public bool FieldEntityExists()
        {
            if (_context.Fields == null)
                return false;

            return true;
        }

        public async Task<bool> FieldExists(int id)
        {
            return await _context.Fields.AnyAsync(m => m.FieldId == id);
        }

        public async Task<bool> SaveChanges()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }
    }
}
