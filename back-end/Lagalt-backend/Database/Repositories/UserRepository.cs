﻿using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace Lagalt_backend.Database.Repositories
{
    /// <summary>
    /// User Repository
    /// </summary>
    public class UserRepository : IUserRepository
    {
        private readonly LagaltDbContext _context;
        public UserRepository(LagaltDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _context.Users.Include(u => u.Projects).Include(u => u.Skills).ToListAsync();
        }

        public async Task<User> GetUserById(int userId)
        {
            return await _context.Users.Where(u => u.UserId == userId).Include(u => u.Projects).Include(u => u.Skills).Include(u => u.UserHistory).FirstOrDefaultAsync();
        }

        public async Task<User> CreateUser(User user)
        {
            try
            {
                _context.Users.Add(user);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return user;
        }

        public async Task<User> GetUserWithSkills(int userId)
        {
            return await _context.Users.Where(u => u.UserId == userId).Include(u => u.Skills).FirstOrDefaultAsync();
        }

        public async Task<bool> UpdateUser(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return true;
        }

        public async Task<bool> SaveChanges()
        {
            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }

        public async Task<User> DeleteUser(int userId)
        {
            var user = await _context.Users.FindAsync(userId);

            if (user != null)
            {
                _context.Users.Remove(user);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            return user;
        }

        public bool UserEntityExists()
        {
            if (_context.Users == null)
                return false;

            return true;
        }
    }
}
