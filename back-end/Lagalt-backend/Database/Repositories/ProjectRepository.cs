﻿using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace Lagalt_backend.Database.Repositories
{
    /// <summary>
    /// Project Repository
    /// </summary>
    public class ProjectRepository : IProjectRepository
    {
        private readonly LagaltDbContext _context;
        public ProjectRepository(LagaltDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Project>> GetAll()
        {
            return await _context.Projects.Include(p => p.Admins).Include(p => p.Members).ToListAsync();
        }

        public async Task<Project> GetProjectById(int id)
        {
            if (_context.Projects == null)
                throw new Exception("There are no projects in Database");

            return await _context.Projects
                .Include(p => p.Admins)
                .Where(f => f.ProjectId == id).FirstOrDefaultAsync();
        }

        public async Task<Project> GetProjectWithMessages(int projectId)
        {
            return await _context.Projects.Where(p => p.ProjectId == projectId).Include(p => p.Messages).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Project>> GetProjectsByKey(string key)
        {
            return await _context.Projects.Where(p => p.ProjectName.Contains(key)).ToListAsync();
        }

        public async Task<IEnumerable<Project>> GetProjectsByFieldId(int fieldId)
        {
            return await _context.Projects.Where(p => p.FieldId == fieldId).ToListAsync();
        }

        public async Task<IEnumerable<User>> GetMembersInProject(int projectId)
        {
            return await _context.Projects.Where(p => p.ProjectId == projectId).Select(p => p.Members).FirstOrDefaultAsync();
        }

        public async Task<bool> UpdateProject(Project project)
        {
            _context.Entry(project).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }

        public async Task<Project> CreateProject(Project project)
        {
            await _context.Projects.AddAsync(project);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return project;
        }

        public async Task<bool> AddMessage(int projectId, Message message)
        {
            Project project = await _context.Projects.FindAsync(projectId);

            if (project.Messages == null)
            {
                project.Messages = new List<Message>();
                project.Messages.Add(message);
            }
            else
            {
                project.Messages.Add(message);
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }

        public async Task<bool> UpdateMembersInProject(int projectId, List<User> members)
        {
            Project project = await _context.Projects.FindAsync(projectId);


            project.Members = members;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return true;
        }

        public async Task<Project> DeleteProject(int id)
        {
            var project = await _context.Projects.FindAsync(id);

            if (project != null)
            {
                _context.Projects.Remove(project);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            return project;
        }

        public async Task<bool> SaveChanges()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }

        public bool ProjectEntityExists()
        {
            if (_context.Projects == null)
                return false;

            return true;
        }

        public async Task<bool> ProjectExists(int id)
        {
            return await _context.Projects.AnyAsync(m => m.ProjectId == id);
        }

        public async Task<bool> UserExistsInProject(int userId, int projectId)
        {
            if (await _context.Projects.Include(p => p.Members.Where(u => u.UserId == userId)).Where(p => p.ProjectId == projectId).FirstOrDefaultAsync() == null)
                return false;

            return true;
        }
    }
}
