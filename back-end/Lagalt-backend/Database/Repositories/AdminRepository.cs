﻿using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace Lagalt_backend.Database.Repositories
{
    /// <summary>
    /// Admin Repository
    /// </summary>
    public class AdminRepository : IAdminRepository
    {
        private readonly LagaltDbContext _context;
        public AdminRepository(LagaltDbContext context)
        {
            _context = context;
        }

        public async Task<List<User>> GetAdminsInProject(int projectId)
        {
            // Get user ids of every admin.
            List<int> adminsUserIds = await _context.Admins.Where(a => a.ProjectId == projectId).Select(a => a.UserId).ToListAsync();

            // return all users with fetched ids
            List<User> admins = new List<User>();
            foreach (var adminUserId in adminsUserIds)
            {
                admins.Add(await _context.Users.Where(u => u.UserId == adminUserId).Include(u => u.Projects).Include(u => u.Skills).FirstOrDefaultAsync());
            }

            return admins;
        }

        public async Task<User> GetAdminInProject(int projectId, int adminId)
        {
            int userId = await _context.Admins.Where(a => a.ProjectId == projectId && a.AdminId == adminId).Select(a => a.UserId).FirstOrDefaultAsync();
            return await _context.Users.Where(u => u.UserId == userId).Include(u => u.Projects).Include(u => u.Skills).FirstOrDefaultAsync();
        }

        public async Task<bool> RemoveAdminRights(int adminId)
        {
            var admin = await _context.Admins.FindAsync(adminId);

            if (admin == null)
                return false;

            _context.Admins.Remove(admin);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }

        public async Task<Admin> CreateAdminInProject(Admin admin)
        {
            try
            {
                _context.Admins.Add(admin);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return admin;
        }

        public async Task<bool> AdminExists(int adminId)
        {
            if (await _context.Admins.FindAsync(adminId) == null)
                return false;

            return true;
        }

        public async Task<bool> AdminExistsInProject(int adminId, int projectId)
        {
            if (await _context.Admins.Where(a => a.AdminId == adminId).Include(a => a.Projects).Where(p => p.ProjectId == projectId).FirstOrDefaultAsync() == null)
                return false;

            return true;
        }

        public bool AdminEntityExists()
        {
            if (_context.Admins == null)
                return false;

            return true;
        }
    }
}
