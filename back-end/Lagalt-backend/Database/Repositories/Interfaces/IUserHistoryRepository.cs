﻿using Lagalt_backend.Model.Domain;

namespace Lagalt_backend.Database.Repositories.Interfaces
{
    /// <summary>
    /// Repository interface for the UserHistory
    /// </summary>
    public interface IUserHistoryRepository
    {
        public Task<bool> CreateUserHistoryOnUser(UserHistory userHistory);
    }
}
