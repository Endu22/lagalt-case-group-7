using Lagalt_backend.Model.Domain;


namespace Lagalt_backend.Database.Repositories.Interfaces
{
    /// <summary>
    /// Repository interface for the Project
    /// </summary>
    public interface IProjectRepository
    {
        public Task<IEnumerable<Project>> GetAll();
        public Task<Project> GetProjectById(int id);
        public Task<Project> GetProjectWithMessages(int projectId);
        public Task<IEnumerable<Project>> GetProjectsByKey(string key);
        public Task<IEnumerable<Project>> GetProjectsByFieldId(int fieldId);
        public Task<IEnumerable<User>> GetMembersInProject(int projectId);
        public Task<bool> UpdateProject(Project project);
        public Task<Project> CreateProject(Project project);
        public Task<bool> AddMessage(int projectId, Message message);
        public Task<Project> DeleteProject(int id);
        public Task<bool> SaveChanges();
        public Task<bool> ProjectExists(int id);
        public bool ProjectEntityExists();
        public Task<bool> UserExistsInProject(int userId, int projectId);
        public Task<bool> UpdateMembersInProject(int projectId, List<User> members);
    }
}
