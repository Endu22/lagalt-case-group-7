﻿using Lagalt_backend.Model.Domain;

namespace Lagalt_backend.Database.Repositories.Interfaces
{
    /// <summary>
    /// Repository interface for the Skill
    /// </summary>
    public interface ISkillRepository
    {
        public Task<IEnumerable<Skill>> GetAll();
        public Task<Skill> GetSkill(int id);
        public Task<bool> UpdateSkill(Skill skill);
        public Task<Skill> CreateSkill(Skill skill);
        public Task<Skill> DeleteSkill(int id);
        public Task<bool> SaveChanges();
        public Task<bool> SkillExists(int id);
        public bool SkillEntityExists();
    }
}
