﻿using Lagalt_backend.Model.Domain;

namespace Lagalt_backend.Database.Repositories.Interfaces
{
    /// <summary>
    /// Repository interface for the User
    /// </summary>
    public interface IUserRepository
    {
        public Task<IEnumerable<User>> GetAll();
        public Task<User> GetUserWithSkills(int projectId);
        public Task<User> GetUserById(int userId);
        public Task<User> CreateUser(User user);
        public Task<bool> UpdateUser(User user);
        public Task<bool> SaveChanges();
        public Task<User> DeleteUser(int userId);
        public bool UserEntityExists();
    }
}
