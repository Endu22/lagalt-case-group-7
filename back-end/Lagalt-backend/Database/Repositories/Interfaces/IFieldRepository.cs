﻿using Lagalt_backend.Model.Domain;

namespace Lagalt_backend.Database.Repositories.Interfaces
{
    /// <summary>
    /// Repository interface for the Field
    /// </summary>
    public interface IFieldRepository
    {
        public Task<IEnumerable<Field>> GetAll();
        public Task<Field> GetAll(int id);
        public Task<Field> Create(Field field);
        public Task<bool> Delete(int id);
        public Task<bool> UpdateField(Field field);
        public Task<Field> FieldToUpdateSkills(int id);
        public Task<List<Skill>> GetAllSkillsInField(int id);
        public Task<bool> FieldExists(int id);
        public bool FieldEntityExists();
        public Task<bool> SaveChanges();
    }
}
