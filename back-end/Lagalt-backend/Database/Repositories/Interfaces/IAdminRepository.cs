﻿using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Admin;
using Microsoft.AspNetCore.Mvc;

namespace Lagalt_backend.Database.Repositories.Interfaces
{
    /// <summary>
    /// Repository interface for the Admin
    /// </summary>
    public interface IAdminRepository
    {
        public Task<List<User>> GetAdminsInProject(int projectId);
        public Task<User> GetAdminInProject(int projectId, int adminId);
        public Task<Admin> CreateAdminInProject(Admin admin);
        public Task<bool> RemoveAdminRights(int adminId);
        public Task<bool> AdminExists(int adminId);
        public Task<bool> AdminExistsInProject(int adminId, int projectId);
        public bool AdminEntityExists();
    }
}
