﻿using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace Lagalt_backend.Database.Repositories
{
    /// <summary>
    /// Skill Repository
    /// </summary>
    public class SkillRepository : ISkillRepository
    {
        private readonly LagaltDbContext _context;
        public SkillRepository(LagaltDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Skill>> GetAll()
        {
            return await _context.Skills.Include(s => s.Users).ToListAsync();
        }

        public async Task<Skill> GetSkill(int id)
        {
            if (_context.Skills == null)
                throw new Exception("There are no skills in Database");

            return await _context.Skills
                .Include(s => s.Users)
                .Where(f => f.SkillId == id).FirstAsync();
        }

        public async Task<bool> UpdateSkill(Skill skill)
        {
            _context.Entry(skill).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }

        public async Task<Skill> CreateSkill(Skill skill)
        {
            await _context.Skills.AddAsync(skill);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return skill;
        }

        public async Task<Skill> DeleteSkill(int id)
        {
            var skill = await _context.Skills.FindAsync(id);

            if (skill != null)
            {
                _context.Skills.Remove(skill);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            return skill;
        }

        public async Task<bool> SaveChanges()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }

        public bool SkillEntityExists()
        {
            if (_context.Skills == null)
                return false;

            return true;
        }

        public async Task<bool> SkillExists(int id)
        {
            return await _context.Skills.AnyAsync(m => m.SkillId == id);
        }
    }
}
