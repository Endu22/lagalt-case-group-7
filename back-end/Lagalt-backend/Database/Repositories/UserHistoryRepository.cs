﻿using Lagalt_backend.Database.Repositories.Interfaces;
using Lagalt_backend.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace Lagalt_backend.Database.Repositories
{
    /// <summary>
    /// UserHistory Repository
    /// </summary>
    public class UserHistoryRepository : IUserHistoryRepository
    {
        private readonly LagaltDbContext _context;
        public UserHistoryRepository(LagaltDbContext context)
        {
            _context = context;
        }

        public async Task<bool> CreateUserHistoryOnUser(UserHistory userHistory)
        {
            try
            {
                _context.UsersHistory.Add(userHistory);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }
    }
}
