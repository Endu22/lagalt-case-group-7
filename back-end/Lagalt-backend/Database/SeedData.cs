﻿using Lagalt_backend.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace Lagalt_backend.Database
{
    public class SeedData
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            #region Users

            //Create Users
            User Oliver = new()
            {
                UserId = 2,
                Firstname = "Oliver",
                Lastname = "Rimmi",
                Username = "OlRi"
            };

            User Daniel = new()
            {
                UserId = 3,
                Firstname = "Daniel",
                Lastname = "Bengtsson",
                Username = "DaBe"
            };

            User Sofia = new()
            {
                UserId = 1,
                Firstname = "Sofia",
                Lastname = "Vulgari",
                Username = "SoVu"
            };

            #endregion

            #region Projects

            // Create Projects
            Project RpgGame = new()
            {
                ProjectId = 1,
                CreatorId = 1,
                ProjectName = "RpgGame",
                Description = "A description for this game"
            };

            Project PokemonTrainer = new()
            {
                ProjectId = 2,
                CreatorId = 2,
                ProjectName = "PokemonTrainer",
                Description = "A description for the pokedex"
            };

            Project EntityFrameworkApi = new()
            {
                ProjectId = 3,
                CreatorId = 3,
                ProjectName = "EntityFrameworkApi",
                Description = "A description for this project"
            };

            #endregion

            #region Fields

            // Create Fields
            Field ITBackend = new()
            {
                FieldId = 1,
                Name = "IT - Backend"
            };

            Field ITFrontEnd = new()
            {
                FieldId = 2,
                Name = "IT - Frontend"
            };

            #endregion

            #region Skills

            // Create Skills
            Skill Csharp = new()
            {
                SkillId = 1,
                SkillName = "C#",
                Description = "BLAH BLAH"
            };

            Skill React = new()
            {
                SkillId = 2,
                SkillName = "React",
                Description = "BLAH BLAH"
            };

            Skill Javascript = new()
            {
                SkillId = 3,
                SkillName = "Javascript",
                Description = "BLAH BLAH"
            };

            #endregion

            #region Seed Entities

            // Seed Users
            modelBuilder.Entity<User>().HasData(Oliver);
            modelBuilder.Entity<User>().HasData(Daniel);
            modelBuilder.Entity<User>().HasData(Sofia);

            // Seed Projects
            modelBuilder.Entity<Project>().HasData(RpgGame);
            modelBuilder.Entity<Project>().HasData(PokemonTrainer);
            modelBuilder.Entity<Project>().HasData(EntityFrameworkApi);

            // Seed Fields
            modelBuilder.Entity<Field>().HasData(ITBackend);
            modelBuilder.Entity<Field>().HasData(ITFrontEnd);

            // Seed Skills
            modelBuilder.Entity<Skill>().HasData(Csharp);
            modelBuilder.Entity<Skill>().HasData(React);
            modelBuilder.Entity<Skill>().HasData(Javascript);

            // Seed linking Table Project - Users. Many to many 
            modelBuilder.Entity<Project>()
              .HasMany(a => a.Members)
               .WithMany(p => p.Projects)
               .UsingEntity<Dictionary<string, object>>(
                   "ProjectUser",
                   r => r
                   .HasOne<User>()
                   .WithMany()
                   .HasForeignKey("UserId"),
                   l => l
                   .HasOne<Project>()
                   .WithMany()
                   .HasForeignKey("ProjectId"),
                   je =>
                   {
                       je.HasKey("UserId", "ProjectId");
                       je.HasData(
                           new { UserId = Oliver.UserId, ProjectId = RpgGame.ProjectId },
                           new { UserId = Daniel.UserId, ProjectId = PokemonTrainer.ProjectId },
                           new { UserId = Sofia.UserId, ProjectId = EntityFrameworkApi.ProjectId }
                       );
                   });

            // Seed linking Table Project - Skills. Many to many
            modelBuilder.Entity<Project>()
              .HasMany(a => a.Skills)
               .WithMany(p => p.Projects)
               .UsingEntity<Dictionary<string, object>>(
                   "ProjectSkills",
                   r => r
                   .HasOne<Skill>()
                   .WithMany()
                   .HasForeignKey("SkillId"),
                   l => l
                   .HasOne<Project>()
                   .WithMany()
                   .HasForeignKey("ProjectId"),
                   je =>
                   {
                       je.HasKey("SkillId", "ProjectId");
                       je.HasData(
                           new { SkillId = Csharp.SkillId, ProjectId = RpgGame.ProjectId },
                           new { SkillId = React.SkillId, ProjectId = PokemonTrainer.ProjectId },
                           new { SkillId = Javascript.SkillId, ProjectId = EntityFrameworkApi.ProjectId }
                       );
                   });
            #endregion
        }
    }
}