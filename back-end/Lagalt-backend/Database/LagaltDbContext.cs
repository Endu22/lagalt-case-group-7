﻿using Lagalt_backend.Model.Domain;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace Lagalt_backend.Database
{
    public class LagaltDbContext : DbContext
    {
        /// <summary>
        /// Database tables
        /// </summary>
        public DbSet<User>? Users => Set<User>();
        public DbSet<UserHistory>? UsersHistory => Set<UserHistory>();
        public DbSet<Project>? Projects => Set<Project>();
        public DbSet<Field>? Fields => Set<Field>();
        public DbSet<Admin>? Admins => Set<Admin>();
        public DbSet<Skill>? Skills => Set<Skill>();
        public DbSet<Message> Messages => Set<Message>();

        public LagaltDbContext([NotNullAttribute] DbContextOptions options) : base(options) { }

        /// <summary>
        /// Seed some initial data
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SeedData.Seed(modelBuilder);
        }
    }
}
