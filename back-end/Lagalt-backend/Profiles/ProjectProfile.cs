﻿using AutoMapper;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Project;

namespace Lagalt_backend.Profiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectReadDTO>()
                .ForMember(p => p.Admins, opt => opt
                .MapFrom(src => src.Admins.Select(a => a.AdminId).ToArray()))
                .ForMember(p => p.Members, opt => opt
                .MapFrom(src => src.Members.Select(u => u.UserId).ToArray()))
                .ForMember(f => f.FieldId, opt => opt
                .MapFrom(src => src.FieldId))
                .ReverseMap();

            CreateMap<Project, ProjectCreateDTO>().ReverseMap();

            CreateMap<Project, ProjectUpdateDTO>().ReverseMap();
        }
    }
}
