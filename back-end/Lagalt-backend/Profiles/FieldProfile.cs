﻿using AutoMapper;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Field;

namespace Lagalt_backend.Profiles
{
    public class FieldProfile : Profile
    {
        public FieldProfile()
        {
            CreateMap<Field, FieldReadDTO>()
                .ForMember(f => f.Skills, opt => opt
                .MapFrom(src => src.Skills.Select(s => s.SkillId).ToArray()))
                .ReverseMap();
            CreateMap<Field, FieldCreateDTO>().ReverseMap();
            CreateMap<Field, FieldUpdateDTO>().ReverseMap();
        }
    }
}
