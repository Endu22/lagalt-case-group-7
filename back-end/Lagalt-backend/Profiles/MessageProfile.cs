﻿using AutoMapper;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Message;

namespace Lagalt_backend.Profiles
{
    public class MessageProfile : Profile
    {
        public MessageProfile()
        {
            CreateMap<Message, MessageReadDTO>().ReverseMap();
            CreateMap<Message, MessageCreateDTO>().ReverseMap();
        }
    }
}
