﻿using AutoMapper;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Admin;

namespace Lagalt_backend.Profiles
{
    public class AdminProfile : Profile
    {
        public AdminProfile()
        {
            CreateMap<Admin, AdminReadDTO>()
                .ForMember(adto => adto.Admins, opt => opt
                .MapFrom(a => a.Admins.Select(u => u.UserId).ToArray()))
                .ForMember(adto => adto.Projects, opt => opt
                .MapFrom(a => a.Projects.Select(p => p.ProjectId).ToArray()))
                .ReverseMap();
        }
    }
}
