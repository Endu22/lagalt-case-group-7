﻿using AutoMapper;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.Skill;

namespace Lagalt_backend.Profiles
{
    public class SkillProfile : Profile
    {
        public SkillProfile()
        {
            CreateMap<Skill, SkillReadDTO>()
                .ForMember(s => s.Users, opt => opt
                .MapFrom(src => src.Users.Select(a => a.UserId).ToArray()))
                .ReverseMap();

            CreateMap<Skill,SkillCreateDTO>().ReverseMap();

            CreateMap<Skill ,SkillUpdateDTO> ().ReverseMap();
        }
    }
}
