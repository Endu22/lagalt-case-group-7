﻿using AutoMapper;
using Lagalt_backend.Model.Domain;
using Lagalt_backend.Model.DTOs.User;

namespace Lagalt_backend.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>()
                .ForMember(u => u.Projects, opt => opt
                .MapFrom(src => src.Projects.Select(p => p.ProjectId).ToArray()))
                .ForMember(u => u.Skills, opt => opt
                .MapFrom(src => src.Skills.Select(s => s.SkillId).ToArray()))
                .ReverseMap();

            CreateMap<User, UserCreateDTO>().ReverseMap();

            CreateMap<User, UserUpdateDTO>().ReverseMap();
        }
    }
}
