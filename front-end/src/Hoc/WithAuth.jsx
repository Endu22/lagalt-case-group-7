import keycloak from '../Keycloak/Keycloak';
import { Navigate } from 'react-router-dom';

//Higher order component to check if there is a authenticated user.
const WithAuth = (Component) => (props) => {
  const authState = keycloak.authenticated;

  if (authState) return <Component {...props} />;
  else return <Navigate to='/login' />;
};

export default WithAuth;
