import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { initialize } from './Keycloak/Keycloak';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <div>
    <p className='flex justify-center mt-5 text-3xl'>
      Please wait while keycloak is loading{''}
      <span className='animate-spin'> ...</span>
    </p>
  </div>
);
initialize().then(() => {
  root.render(<App />);
});
