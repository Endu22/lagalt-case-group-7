import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import MainView from "./Views/MainView";
import UserProfile from "./Views/UserProfile";
import Navbar from "./Components/Navbar";
import Project from "./Components/Project/Project";
import { ProjectsContext } from "./Context/ProjectsContext";
import Footer from "./Components/Footer/Footer";
import Login from "./Components/Login/Login";
import ProjectList from "./Components/ProjectList/ProjectList";
import { CreateUserAccount } from "./Components/CreateUserAccount/CreateUserAccount";
import keycloak from "./Keycloak/Keycloak";
import AdminView from "./Views/AdminView";

function App() {
    if (keycloak.authenticated) {
        CreateUserAccount();
    }
    return (
        <BrowserRouter>
            <Navbar />
            <div className="container mx-auto">
                <ProjectsContext>
                    <Routes>
                        <Route path="/" element={<MainView />} />
                        <Route path="/profile" element={<UserProfile />} />
                        <Route path="/projects" element={<ProjectList />} />
                        <Route path="/projects/:id" element={<Project />} />
                        <Route path="/Login" element={<Login />} />
                        <Route path="/Admin" element={<AdminView />} />
                    </Routes>
                </ProjectsContext>
            </div>
            <Footer />
        </BrowserRouter>
    );
}

export default App;
