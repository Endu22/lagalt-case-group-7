import { ContextData } from "../Context/ProjectsContext";
import { createHeaders, createHeadersWithToken } from "./headers";

const apiUrl = process.env.REACT_APP_SERVER_ENDPOINT;

export const updateUserHistory = async (projectId) => {
    const user = JSON.parse(localStorage.getItem("user"));
    const reqOptions = {
        method: "PUT",
        headers: createHeadersWithToken(),
        body: JSON.stringify({
            userId: user.userId,
            projectId: projectId,
        }),
    };

    const response = await fetch(
        `${apiUrl}/users/history/${user.userId}`,
        reqOptions
    );
    return response;
};
