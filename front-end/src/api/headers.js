import keycloak from "../Keycloak/Keycloak";

export const createHeaders = () => {
    return {
        "Content-type": "application/json",
    };
};

export const createHeadersWithToken = () => {
    return {
        "Content-type": "application/json",
        Authorization: `bearer ${keycloak.token}`
    }
}