import { createHeaders, createHeadersWithToken } from "./headers";

const apiUrl = process.env.REACT_APP_SERVER_ENDPOINT;
//Function to search for a project. get value from input field.
export const searchForProject = async (searchString) => {
    try {
        //use value user has typed as variable to get the projects searched for.
        const response = await fetch(
            `${apiUrl}/projects/search/${searchString}`
        );
        if (!response.ok) {
            throw new Error("An error occurred while searching for projects.");
        }
        const data = await response.json();
        return [null, data];
    } catch (error) {
        return [error.message, []];
    }
};
//Funciton to get all the messages in a project.
export const getAllMessagesInProject = async (projectId) => {
    try {
        const response = await fetch(
            `${apiUrl}/projects/messages/${projectId}`
        );
        if (!response.ok) {
            throw new Error(
                "An error occurred while fetching messages in project."
            );
        }
        const data = await response.json();
        return [null, data];
    } catch (error) {
        return [error.message, []];
    }
};
//Get all the members in a project
export const getAllMembersInProject = async (projectId) => {
    try {
        const response = await fetch(`${apiUrl}/projects/${projectId}/members`);
        if (!response.ok) {
            throw new Error(
                "An error occurred while fetching members in project."
            );
        }
        const data = await response.json();
        return [null, data];
    } catch (error) {
        return [error.message, []];
    }
};
//Funciton to post a message in a project.
export const postMessageInProject = async (projectId, userId, messageText) => {
    try {
        const response = await fetch(`${apiUrl}/projects/${projectId}`, {
            method: "POST",
            headers: createHeadersWithToken(),
            body: JSON.stringify({
                messageText: messageText,
                messageCreatorId: userId,
            }),
        });

        if (!response.ok) {
            throw new Error(
                "Something went wrong when attempting to post a message."
            );
        }
        const data = response;
        return [null, data];
    } catch (error) {
        return [error.message, []];
    }
};

//add the clicked member project to user.
export const postProjectToUser = async (id, userId) => {
    try {
        await fetch(`${apiUrl}/projects/members/${id}`, {
            method: "PUT",
            headers: createHeadersWithToken(),
            body: JSON.stringify(userId),
        });
    } catch (error) {
        return error;
    }
};

//function that posts a project to the database.
export const createProjectInDB = async (project) => {
    let success = false;

    try {
        const res = await fetch(`${apiUrl}/projects`, {
            method: "POST",
            headers: createHeadersWithToken(),
            body: JSON.stringify(project),
        });
        if (!res.ok) {
            throw new Error("Something went wrong trying to create a project");
        }
        success = true;
        return [success, null];
    } catch (error) {
        return [null, error];
    }
};

//Get the History of a user

export const getSortedHistory = async (userId) => {
    try {
        const res = await fetch(`${apiUrl}/projects/sorted/${userId}`);
        if (!res.ok) throw new Error("Something went wrong fetching data.");
        const data = await res.json();
        const sortedHistory = await data;
        return [sortedHistory, null];
    } catch (error) {
        return [null, error.message];
    }
};
