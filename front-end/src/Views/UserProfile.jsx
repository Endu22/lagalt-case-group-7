
import CreateProject from "../Components/Profile/CreateProject";
import WithAuth from "../Hoc/WithAuth";
import AddSkills from "../Components/Profile/AddSkills";

const UserProfile = () => {

    return (
        <div className="Profile">
            <title>Profile</title>
            <AddSkills/>
            <CreateProject />
        </div>
    );
};

export default WithAuth(UserProfile);
