import { useEffect, useState } from 'react';
import WithAuth from '../Hoc/WithAuth';
import { Link } from 'react-router-dom';
import Fields from '../Components/ProjectList/Fields';

const AdminView = () => {
  const [apiProjects, setApiProjects] = useState([]);
  const [projectsMember, setProjectsMembers] = useState([]);
  const [loading, setLoading] = useState(false);
  const projectsInStorage = JSON.parse(localStorage.getItem('projects'));
  const userInStorage = JSON.parse(localStorage.getItem('user'));
  const fields = JSON.parse(localStorage.getItem('fields'));
  let memberProjects = [];
  let projectDataArray = [];

  useEffect(() => {
    //Looping through the localstorage to get all the project that belongs to the user.
    for (let i = 0; i < projectsInStorage.length; i++) {
      if (projectsInStorage[i].creatorId === userInStorage.userId) {
        projectDataArray.push(projectsInStorage[i]);
      }
    }
    //looping through localstorage to get all the projects that a user is a member of.
    for (let i = 0; i < projectsInStorage.length; i++) {
      for (let j = 0; j < projectsInStorage[i].members.length; j++) {
        if (projectsInStorage[i].members[j] === userInStorage.userId) {
          memberProjects.push(projectsInStorage[i]);
        }
      }
    }
    //UseState to save the data inside.
    setProjectsMembers(memberProjects);
    setApiProjects(projectDataArray);
  }, []);

  return (
    <div>
      <p className='text-2xl font-semibold'>Created projects</p>

      <div className='grid grid-cols-2 gap-2'>
        {apiProjects &&
          apiProjects.map((data, index) => {
            return (
              <div
                key={index}
                className='bg-slate-400 p-2 border-y-8 border-x-8'
              >
                <Link to={`/projects/${data.projectId}`}>
                  <p className='text-xl'>{data.projectName}</p>
                  <hr></hr>
                  <p>{data.description}</p>
                  <div>
                    <Fields projects={data} index={index} fields={fields} />
                  </div>
                </Link>
              </div>
            );
          })}
      </div>
      <p className='text-2xl mt-3 font-semibold'>Member projects</p>
      <div className='grid grid-cols-2 gap-2 mt-1'>
        {projectsMember &&
          projectsMember.map((data, i) => {
            return (
              <div key={i} className='bg-slate-400 p-2 '>
                <Link to={`/projects/${data.projectId}`}>
                  <p className='text-xl'>{data.projectName}</p>
                  <hr></hr>
                  <p>{data.description}</p>
                  <Fields projects={data} index={i} fields={fields} />
                </Link>
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default WithAuth(AdminView);
