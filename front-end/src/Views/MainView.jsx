import ProjectList from "../Components/ProjectList/ProjectList";

const MainView = () => {
    return (
        <div>
            <ProjectList />
        </div>
    );
};

export default MainView;
