import { useState } from 'react';
import { searchForProject } from '../../api/projects';
import SearchProject from './SearchProject';

const Search = () => {
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);
  const [projects, setProjects] = useState(null);

  //handle input onChange.
  const handleSearchSubmit = async (key) => {
    setLoading(true);
    //Imported function that handles the input value.
    const [error, parsedProjects] = await searchForProject(key.value);

    //If there is an error fetching the data. set a useState with the error.
    if (error) setApiError(error);

    //if projects is not empty. set a useState to the projects found.
    if (parsedProjects != null) {
      setProjects(parsedProjects);
    }

    setLoading(false);
  };
  //Functions that renders the projects searched for.
  const renderReceivedProjects = () => {
    return (
      <div className='h-60 max-h-96 overflow-auto bg-slate-300'>
        {projects.map((project) => (
          <SearchProject key={project.projectId} project={project} />
        ))}
      </div>
    );
  };

  return (
    <div className='bg-slate-300 h-80 mb-2'>
      <p className='m-2 mt-0 pt-1'>Search projects:</p>
      <input
        className='ml-2 border-2 rounded-lg w-96'
        type='text'
        placeholder='e.g., Skilled Musicians'
        onChange={(e) => handleSearchSubmit(e.target)}
      ></input>
      <div className='h-2 m-2'>{loading && <p>loading</p>}</div>
      {projects && renderReceivedProjects()}
      {apiError && <p>{apiError.message}</p>}
    </div>
  );
};
export default Search;
