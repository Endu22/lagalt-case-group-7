import { useState } from 'react';
import Search from './Search';
import Modal from 'react-modal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';

//Modal styles.
const customStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    display: 'flex',
  },
  content: {
    position: 'absolute',
    top: '40%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    border: '1px solid #ccc',
    background: '#fff',
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '4px',
    outline: 'none',
    padding: '10px',
    transform: 'translate(-50%, -50%)',
    width: '800px',
  },
};

const SearchButton = () => {
  const [modalIsOpen, setIsOpen] = useState(false);

  //Open modal if search button is pressed
  const toggleSearchButton = () => {
    setIsOpen(!modalIsOpen);
  };

  return (
    <div>
      <button
        className='hover:text-white text-xl p-3 flex '
        onClick={toggleSearchButton}
      >
        <FontAwesomeIcon icon={faMagnifyingGlass} />
        <p className='ml-2 font-mono text-xl '>Search</p>
      </button>

      <Modal
        className=''
        isOpen={modalIsOpen}
        style={customStyles}
        ariaHideApp={false}
        shouldCloseOnOverlayClick={true}
        onRequestClose={toggleSearchButton}
      >
        <button
          className='float-right mr-1 p-0 mt-0 rounded-full hover:bg-gray-200 w-6'
          onClick={toggleSearchButton}
        >
          X
        </button>
        <Search />
      </Modal>
    </div>
  );
};
export default SearchButton;
