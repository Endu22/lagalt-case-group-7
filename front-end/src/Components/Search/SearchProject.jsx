const SearchProject = ({ project }) => {
  return (
    <div className="p-2 m-2 border-2 hover:border-black">
      <a href={`/projects/${project.projectId}`}>
        <p>{project.projectName}</p>
      </a>
    </div>
  )
}
export default SearchProject
