import React from "react";

const Footer = () => {
    return (
        <footer className=" bg-gray-100 text-gray-600 mt-2 mt-96 w-full">
            <div className="flex justify-center items-center lg:justify-between p-6 border-b border-gray-300">
                Lagalt Project
            </div>
            <div className="mx-6 py-10 text-center md:text-left">
                <div className="grid grid-1 md:grid-cols-2 lg:grid-cols-4 gap-8">
                    <div className="">
                        <h6 className="uppercase font-semibold mb-4 flex items-center justify-center md:justify-start">
                            About Lagalt
                        </h6>
                        <p className="mb-4">
                            <a
                                href="https://gitlab.com/Endu22/lagalt-case-group-7/-/blob/main/README.md"
                                className="text-gray-600"
                            >
                                Readme
                            </a>
                        </p>
                        <p className="mb-4">
                            <a
                                href="https://gitlab.com/Endu22/lagalt-case-group-7"
                                className="text-gray-600"
                            >
                                Repository
                            </a>
                        </p>
                        <p className="mb-4">
                            <a href="#!" className="text-gray-600">
                                Documentation
                            </a>
                        </p>
                        <p>
                            <a href="#!" className="text-gray-600">
                                Demo
                            </a>
                        </p>
                    </div>
                    <div className="">
                        <h6 className="uppercase font-semibold mb-4 flex justify-center md:justify-start">
                            Contact Oliver
                        </h6>
                        <a
                            href="mailto:oliver.rimmi@se.experis.com"
                            className=" text-gray-600"
                        >
                            <p className="flex items-center justify-center md:justify-start mb-4">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="envelope"
                                    className="w-4 mr-4"
                                    role="img"
                                    xmlns="oliver.rimmi@se.experis.com"
                                    viewBox="0 0 512 512"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"
                                    ></path>
                                </svg>
                                Oliver Rimmi
                            </p>
                        </a>
                        <a href="#!" className=" text-gray-600">
                            <p className="flex items-center justify-center md:justify-start mb-4">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="linkedin-in"
                                    className="w-4 mr-4"
                                    role="img"
                                    xmlns=""
                                    viewBox="0 0 576 512"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"
                                    ></path>
                                </svg>
                                LinkedIn
                            </p>
                        </a>
                        <a
                            href="https://gitlab.com/Endu22"
                            className=" text-gray-600"
                        >
                            <p className="flex items-center justify-center md:justify-start mb-4">
                                <svg
                                    xmlns="https://gitlab.com/Endu22"
                                    className="icon icon-tabler icon-tabler-brand-gitlab w-5 mr-5"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    strokeWidth="2"
                                    stroke="currentColor"
                                    fill="none"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                >
                                    <path
                                        stroke="none"
                                        d="M0 0h24v24H0z"
                                        fill="none"
                                    />{" "}
                                    <path d="M21 14l-9 7l-9 -7l3 -11l3 7h6l3 -7z" />
                                </svg>
                                GitLab
                            </p>
                        </a>
                    </div>
                    <div className="">
                        <h6 className="uppercase font-semibold mb-4 flex justify-center md:justify-start">
                            Contact Daniel
                        </h6>
                        <a
                            href="mailto:daniel.johan.gunnar.bengtsson@se.experis.com"
                            className=" text-gray-600"
                        >
                            <p className="flex items-center justify-center md:justify-start mb-4">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="envelope"
                                    className="w-4 mr-4"
                                    role="img"
                                    xmlns="daniel.johan.gunnar.bengtsson@se.experis.com"
                                    viewBox="0 0 512 512"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"
                                    ></path>
                                </svg>
                                Daniel Bengtsson
                            </p>
                        </a>
                        <a href="#!" className=" text-gray-600">
                            <p className="flex items-center justify-center md:justify-start mb-4">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="linkedin-in"
                                    className="w-4 mr-4"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 576 512"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"
                                    ></path>
                                </svg>
                                LinkedIn
                            </p>
                        </a>
                        <a
                            href="https://gitlab.com/DanielBengtsson"
                            className=" text-gray-600"
                        >
                            <p className="flex items-center justify-center md:justify-start mb-4">
                                <svg
                                    xmlns="https://gitlab.com/DanielBengtsson"
                                    className="icon icon-tabler icon-tabler-brand-gitlab w-5 mr-5"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    strokeWidth="2"
                                    stroke="currentColor"
                                    fill="none"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                >
                                    <path
                                        stroke="none"
                                        d="M0 0h24v24H0z"
                                        fill="none"
                                    />{" "}
                                    <path d="M21 14l-9 7l-9 -7l3 -11l3 7h6l3 -7z" />
                                </svg>
                                GitLab
                            </p>
                        </a>
                    </div>
                    <div className="">
                        <h6 className="uppercase font-semibold mb-4 flex justify-center md:justify-start">
                            Contact Sofia
                        </h6>
                        <a
                            href="mailto:sofia.vulgari@se.experis.se"
                            className=" text-gray-600"
                        >
                            <p className="flex items-center justify-center md:justify-start mb-4">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="envelope"
                                    className="w-4 mr-4"
                                    role="img"
                                    xmlns="sofia.vulgari@se.experis.se"
                                    viewBox="0 0 512 512"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"
                                    ></path>
                                </svg>
                                Sofia Vulgari
                            </p>
                        </a>
                        <a href="#!" className=" text-gray-600">
                            <p className="flex items-center justify-center md:justify-start mb-4">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="linkedin-in"
                                    className="w-4 mr-4"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 576 512"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"
                                    ></path>
                                </svg>
                                LinkedIn
                            </p>
                        </a>
                        <a
                            href="https://gitlab.com/SofiaVulgari"
                            className=" text-gray-600"
                        >
                            <p className="flex items-center justify-center md:justify-start mb-4">
                                <svg
                                    xmlns="https://gitlab.com/SofiaVulgari"
                                    className="icon icon-tabler icon-tabler-brand-gitlab w-5 mr-5"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    strokeWidth="2"
                                    stroke="currentColor"
                                    fill="none"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                >
                                    <path
                                        stroke="none"
                                        d="M0 0h24v24H0z"
                                        fill="none"
                                    />{" "}
                                    <path d="M21 14l-9 7l-9 -7l3 -11l3 7h6l3 -7z" />
                                </svg>
                                GitLab
                            </p>
                        </a>
                    </div>
                </div>
            </div>
            <div className="text-center p-6 bg-gray-200">
                <span>© 2022 Copyright </span>
                <a className="text-gray-600 font-semibold" href=" "></a>
            </div>
        </footer>
    );
};
export default Footer;
