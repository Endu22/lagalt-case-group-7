import keycloak from "../Keycloak/Keycloak";
import SearchButton from "./Search/SearchButton";
import LoginBtn from "./Login/LoginBtn";
import { Link } from "react-router-dom";

const Navbar = () => {
    return (
        <div className="bg-slate-400 flex justify-between h-14 px-5">
            <div>
                <Link to="/" className="text-3xl font-extrabold">
                    Lagalt
                </Link>
            </div>

            <div>
                <SearchButton />
            </div>

            <div className="flex ">
                {keycloak.authenticated ? <Link className="mt-3" to="/admin">
                    Your projects
                </Link>: null}
                <Link
                    to="/profile"
                    className="mt-3 font-semibold px-4 underline"
                >
                    {keycloak.tokenParsed &&
                        keycloak.tokenParsed.preferred_username}
                </Link>
                <LoginBtn btnText={"Login"} />
            </div>
        </div>
    );
};

export default Navbar;
