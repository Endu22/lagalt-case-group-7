import { useState } from 'react';
import { postMessageInProject } from '../../api/projects';
import { ContextData } from '../../Context/ProjectsContext';

const MessageCreator = (props) => {
  const [textArea, setTextArea] = useState(null);
  const [apiError, setApiError] = useState(null);
  //get user from context.
  const { user } = ContextData();
  //post message function.
  const postMessage = async (e) => {
    e.preventDefault();

    //Post message function from api/projects. post message with value and user id.
    const [error, parsedResponse] = await postMessageInProject(
      props.projectId,
      user.userId,
      textArea.value
    );

    if (error !== null) setApiError(error);

    textArea.value = '';
    props.handleCounter();
  };

  return (
    <form className='ml-2 mt-2' onSubmit={(e) => postMessage(e)}>
      <label>Create a message</label>
      <div className='flex mt-1'>
        <textarea
          onChange={(e) => setTextArea(e.target)}
          className='align-middle w-4/5 resize-none mr-1 h-28 p-1'
          type='text'
          name='messageCreation'
        />
        <input
          className='ml-1 mr-2 bg-slate-200 hover:bg-emerald-200 hover:shadow-lg h-28 w-16 '
          type='submit'
          value='Post'
        />
      </div>
      {apiError && <p>{apiError}</p>}
    </form>
  );
};
export default MessageCreator;
