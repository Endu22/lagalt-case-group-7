import { useEffect, useState } from 'react';
import '../../App.css';

const ProjectChatEntry = (props) => {
  const [member, setMember] = useState(null);

  useEffect(() => {
    //Get the username of the user that created a message.
    const getUsername = (messageCreatorId) => {
      if (props.members != null) {
        props.members.forEach((member) => {
          if (member.userId === messageCreatorId) {
            setMember(member);
          }
        });
      }
    };
    getUsername(props.message.messageCreatorId);
  });

  return (
    <div className='m-2 mb-3 bg-white'>
      {member && (
        <p className='m-2 text-center border-b-2'>{member.username}</p>
      )}
      <pre className='m-2 text-xl'>{props.message.messageText}</pre>
    </div>
  );
};
export default ProjectChatEntry;
