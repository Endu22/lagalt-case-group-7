import { useEffect, useState } from 'react';
import {
  getAllMembersInProject,
  getAllMessagesInProject,
} from '../../api/projects';
import ProjectChatEntry from './ProjectChatEntry';
import keycloak from '../../Keycloak/Keycloak';
import MessageCreator from './MessageCreator';

const ProjectChat = ({ projectId }) => {
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);
  const [messages, setMessages] = useState(null);
  const [members, setMembers] = useState(null);
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    setLoading(true);

    //Get all the messages in a project.
    const fetchAllMessagesInProject = async () => {
      const [error, parsedChatObjects] = await getAllMessagesInProject(
        projectId
      );

      if (error) setApiError(error);

      if (parsedChatObjects != null) setMessages(parsedChatObjects.reverse());

      setLoading(false);
    };

    fetchAllMessagesInProject();
  }, [counter]);

  useEffect(() => {
    setLoading(true);
    //Get members in a project.
    const fetchMembersInProject = async () => {
      const [error, parsedMembersInProject] = await getAllMembersInProject(
        projectId
      );
      //if there is an error, set a useState with the error.
      if (error) setApiError(error);
      //set the members in project to a useState.
      if (parsedMembersInProject) setMembers(parsedMembersInProject);

      setLoading(false);
    };

    fetchMembersInProject();
  }, []);

  //Loop through messages and return a component.
  const getAllExistingMessages = () => {
    return (
      <div className='pt-2'>
        {messages.map((message) => (
          <ProjectChatEntry
            key={message.messageId}
            message={message}
            members={members}
          />
        ))}
      </div>
    );
  };

  //used to remount component, to instantly see the new message.
  const handleCounter = () => {
    setCounter(counter + 1);
  };

  return (
    <div className='m-2 border-t-2 w-3/6 '>
      {keycloak && keycloak.authenticated && (
        <MessageCreator projectId={projectId} handleCounter={handleCounter} />
      )}
      {loading && <p>Loading...</p>}
      {messages && getAllExistingMessages()}
      {apiError && <p>{apiError.message}</p>}
    </div>
  );
};
export default ProjectChat;
