import JoinProject from "../BecomeMemberBtn/JoinProject";
import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import keycloak from "../../Keycloak/Keycloak";
import LoginBtn from "../Login/LoginBtn";
import ProjectStats from "./ProjectStats";
import ProjectChat from "./ProjectChat";
import { updateUserHistory } from "../../api/users";
import { ContextData, updateStorage } from "../../Context/ProjectsContext";

const apiUrl = process.env.REACT_APP_SERVER_ENDPOINT;

const Project = () => {
    const [project, setProject] = useState([]);
    const { id } = useParams();

    const updateUserHistoryPut = async () => {
        await updateStorage();
        await updateUserHistory(id);
    };

    useEffect(() => {
        try {
            fetch(`${apiUrl}/projects/${id}`)
                .then((res) => res.json())
                .then((data) => setProject(data))
                .catch((err) => console.log(err));
            console.log("Data fetched");
        } catch (error) {
            console.log(error + "Something went wrong");
        }
        try {
            updateUserHistoryPut();
        } catch (error) {
            console.log(error);
        }
    }, []);

    return (
        <div>
            {project.length !== 0 ? (
                <div className="mt-5 p-2 bg-slate-400 rounded-xl">
                    <div className="flex justify-between">
                        <h1 className="text-3xl">{project.projectName}</h1>

                        <ProjectStats id={id} />
                    </div>
                    <hr />
                    <h1 className="text-3xl">{project.description}</h1>

                    <div className="flex justify-end">
                        {keycloak && keycloak.authenticated ? (
                            <JoinProject projectId={project.projectId} />
                        ) : (
                            <LoginBtn btnText={"Login to become a member"} />
                        )}
                    </div>

                    <div className="flex justify-center">
                        <ProjectChat projectId={project.projectId} />
                    </div>
                </div>
            ) : (
                <div className="text-center mt-2">
                    <p className="text-3xl ">This project no longer exists</p>
                    <Link to="/" className="hover:underline">
                        Go to Projects
                    </Link>
                </div>
            )}
        </div>
    );
};

export default Project;
