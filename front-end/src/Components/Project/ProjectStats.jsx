import { useState, useEffect } from "react";
import { ContextData } from "../../Context/ProjectsContext";

const apiUrl = process.env.REACT_APP_SERVER_ENDPOINT;

const ProjectStats = ({ id }) => {
    const { projects } = ContextData();
    const [user, setUser] = useState();
    let project;
    let tmpUser;

    let url = `${apiUrl}/users`;

    useEffect(() => {
        const fetchUsers = async () => {
            try {
                let res = await fetch(url);
                let data = await res.json();
                await data.forEach((users, i) => {
                    if (users.userId === project.creatorId) {
                        tmpUser = users;
                    }
                });
                setUser(tmpUser);
            } catch (error) {
                console.table(error);
            }
        };
        console.log("data");
        fetchUsers();
    }, [projects]);

    projects.forEach((element) => {
        if (element.projectId === parseInt(id)) {
            project = element;
        }
    });

    return (
        <div className="flex">
            <p className="font-medium">Project Creator:</p>
            <span className="underline">{user && user.username}</span>
        </div>
    );
};

export default ProjectStats;
