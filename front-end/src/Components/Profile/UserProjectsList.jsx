import {  Link } from "react-router-dom";

const UserProjectsList = () => {
    return (
        <center>
                <div className="p-4 space-y-10 mt-1">
                <div className="inline-block w-100 h-40 p-16 bg-stone-200 rounded-lg overflow-x-auto text-justify">
                    <div className="grid grid-cols-1 mt-0">
                    <div className="block w-40 h-20 p-2 bg-stone-400 rounded-lg">
                        <p>project 1</p>
                        <Link to="/" className="" > Go to project </Link>
                        </div>
                    </div>
                </div>
                <span className="spacer mx-10"></span>
                <div className="inline-block w-100 h-40 p-16 bg-stone-200 rounded-lg overflow-x-auto text-justify">
                <div className="grid grid-cols-1">
                    <div className="block w-40 h-20 p-2 bg-stone-400 mx-1 rounded-lg">
                        <p>project 5</p>
                        <Link to="/" className="" > Go to project </Link>
                    </div>
                </div>
                </div>
                </div> 
                </center>
    );
};

export default UserProjectsList;
