import React, { useState, useEffect } from "react";
import keycloak from "../../Keycloak/Keycloak";

const apiUrl = process.env.REACT_APP_SERVER_ENDPOINT

const AddSkills = () => {
   
   const [skills, setSkills] = useState([]);
   const [userSkills, setUserSkills] = useState([]);
   const [user, setUser] = useState([]);
   const [loading, setLoading] = useState(true);

   useEffect(() => {
       const fetchSkills = async () => {
           const res = await fetch(`${apiUrl}/skills`);
           const data = await res.json();
           setSkills(data);
           setLoading(!loading);
           console.log("Fetched Skill Data");
       }

       fetchSkills();

       const fetchUserSkills = async () => {
       let userExist = false;
       let skillExist = false;
       let users = [];
       let userId;
       let tempSkills = [];

       await fetch(`${apiUrl}/users`)
           .then((res) => res.json())
           .then((data) => users.push(data));

       for (let i = 0; i < users[0].length; i++) {
           if (
               users[0][i].username === keycloak.tokenParsed.preferred_username
           ) {
               userExist = true;
               userId = users[0][i].userId;
           }
       }
       
       if (userExist){
           await fetch(`${apiUrl}/users/${userId}`)
            .then((res) => res.json())
            .then((data) => {
                setUser(data);
                setLoading(!loading);
                /* console.log(data);
                console.log(skills) */
            });

           for (let i=0; i<skills.length; i++ ){
               if (skills[i].skillId === user.skills[i]){
                   tempSkills.push(user.skills[i]);
               }
               setUserSkills(tempSkills)
               console.log(tempSkills);
           }
          
       }
       }

       fetchUserSkills();
   }, []);

   const handleSkillClick = async (skill) => {
       let skillId = skill.skillId;
       let userExist = false;
       let skillExist = false;
       let users = [];
       let userId;

       await fetch(`${apiUrl}/users`)
           .then((res) => res.json())
           .then((data) => users.push(data));

       for (let i = 0; i < users[0].length; i++) {
           if (
               users[0][i].username === keycloak.tokenParsed.preferred_username
           ) {
               userExist = true;
               userId = users[0][i].userId;
           }
       }

       if (userExist) {
           await fetch(`${apiUrl}/Users/skills/${userId}`,
               {
                   method: "PUT",
                   headers: { "Content-Type": "application/json" },
                   body: JSON.stringify([skillId]),
               }
           ).then((data) => console.log(data));
           skillExist = true;
       }

   }

   return (
       <div>
       <p>Add Skills:</p> 
                   <div className="inline-flex mr-96 rounded-md shadow-sm" role="group">
                       {skills && skills.map((data, index) => {
                           return (
                               <div key={index}>
                               <button onClick={() =>
                                       handleSkillClick(data)
                                   } className="ml-1 bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow">
                               {data.skillName}
                               </button>
                               </div>
                           )
                       })}
                   </div>
       <p>My Skills:</p>
               <div className="inline-flex mr-96 rounded-md shadow-sm" role="group">
                       {userSkills && userSkills.map((data, index) => {
                           return (
                               <div key={index}>
                               <button className="ml-1 bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow">
                               {data.skillName}
                               </button>
                               </div>
                           )
                       })}
                   </div>
       </div>
   );
};

export default AddSkills;
