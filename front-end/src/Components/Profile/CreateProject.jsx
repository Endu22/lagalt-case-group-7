import { useContext } from 'react';
import { useState } from 'react';
import { ContextData } from '../../Context/ProjectsContext';
import { updateStorage } from '../../Context/ProjectsContext';
import { createProjectInDB } from '../../api/projects';

const CreateProject = () => {
  const { user } = ContextData();

  //UseState
  const [projectName, setName] = useState('');
  const [description, setDescription] = useState('');
  const [success, setSuccess] = useState(false);
  const [fieldId, setFieldId] = useState(null);
  const [error, setError] = useState(null);

  let fieldsInStorage = JSON.parse(localStorage.getItem('fields'));

  //Handle description input
  function handleDescription(e) {
    setDescription(e.target.value);
  }
  //Handle project name input
  function handleProjectName(e) {
    setName(e.target.value);
  }
  const createProject = async () => {
    //Create a project object.
    const project = {
      creatorId: user.userId,
      projectName: projectName,
      description: description,
      fieldId: fieldId,
    };
    //function to create project.
    let [success, errMsg] = await createProjectInDB(project);
    setError(errMsg);
    setSuccess(success);
    //Update storage with the new data.
    await updateStorage();
    //A timeout funciton that removes the success message after 1.5 Sec.
    setTimeout(() => {
      setSuccess(false);
    }, 1500);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    await createProject();
  };

  const handleSelectChange = (e) => {
    let fieldIdInt = parseInt(e.target.value);
    console.log(fieldIdInt);
    setFieldId(fieldIdInt);
  };

  return (
    <div className='mt-10 bg-slate-400 p-2 rounded-md '>
      <p className='font-semibold text-3xl font-mono text-center'>
        Create a new Project
      </p>
      <form onSubmit={handleSubmit} className='grid '>
        <div className='grid content-center justify-center mt-2'>
          <label className='block text-gray-700 text-sm font-bold mb-2'>
            Project Name
          </label>
          <input
            onChange={handleProjectName}
            className='shadow appearance-none border rounded sm:w-96 py-2 px-3 text-gray-700 leading-tight focus:outline-4 focus:outline-emerald-300'
            type='text'
            name='Projectname'
            required
            autoComplete='off'
            placeholder='Name of the project'
          />
        </div>
        <br />
        <div className=''>
          <div className='grid justify-center'>
            <label className='block text-gray-700 text-sm font-bold mb-2'>
              Project Description
            </label>
            <input
              onChange={handleDescription}
              className='shadow appearance-none border rounded sm:w-96  py-2 px-3 text-gray-700 leading-tight focus:outline-4 focus:outline-emerald-300'
              type='text'
              name='Description'
              required
              autoComplete='off'
              placeholder='Description of the project'
            />
          </div>
        </div>
        <div className='grid justify-center '>
          <select
            className='mt-2 border-x-4 border-y-4 rounded-md p-1 active:outline-emerald-400'
            name='Fields'
            required
            onChange={handleSelectChange}
          >
            <option value='' hidden>
              Choose a field
            </option>
            {fieldsInStorage &&
              fieldsInStorage.map((data, i) => {
                return (
                  <option value={data.fieldId} key={i}>
                    {data.name}
                  </option>
                );
              })}
          </select>
        </div>
        <br />
        <input
          className='ml-1 hover:cursor-pointer bg-slate-300 hover:bg-gray-100 text-gray-800 font-semibold py-1 px-2 border border-gray-400 rounded shadow'
          type='submit'
          value='Submit'
        />
      </form>
      {error && error.message}
      {success ? (
        <div className='bg-green-400 p-5 rounded-xl text-center mt-5'>
          <p className='font-mono font-bold text-3xl'>
            {' '}
            Project created Successfully.
          </p>
        </div>
      ) : null}
    </div>
  );
};
export default CreateProject;
