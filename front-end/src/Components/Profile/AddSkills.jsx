import React, { useState, useEffect } from 'react';
import { createHeadersWithToken } from '../../api/headers';
import keycloak from '../../Keycloak/Keycloak';

const apiUrl = process.env.REACT_APP_SERVER_ENDPOINT;

const AddSkills = () => {
  //UseStaes
  const [skills, setSkills] = useState([]);
  const [userSkills, setUserSkills] = useState([]);
  const [loading, setLoading] = useState(true);
  const userInStorage = JSON.parse(localStorage.getItem('user'));

  useEffect(() => {
    //Get Skills
    const fetchSkills = async () => {
      const res = await fetch(`${apiUrl}/skills`, {
        headers: createHeadersWithToken(),
      });
      const data = await res.json();

      setSkills(data);

      let userId = userInStorage.userId;

      const skillRes = await fetch(`${apiUrl}/Users/skills/${userId}`);
      const skillData = await skillRes.json();

      let tempUserSkills = await skillData;

      setUserSkills(tempUserSkills);
      console.log('UseEffect inside skills component');
    };

    fetchSkills();
  }, [loading]);
  //Handle click on a skill
  const handleSkillClick = async (skill) => {
    let skillId = skill.skillId;
    let shouldPut = false;
    let userExist = false;
    let users = null;
    let userId;
    let user = null;

    await fetch(`${apiUrl}/users`)
      .then((res) => res.json())
      .then((data) => (users = data));

    for (let i = 0; i < users.length; i++) {
      if (users[i].username === keycloak.tokenParsed.preferred_username) {
        userExist = true;
        userId = users[i].userId;
        if (!users[i].skills.includes(skillId)) {
          users[i].skills.push(skillId);
          shouldPut = true;
        }
        user = users[i];
      }
    }

    if (userExist && shouldPut) {
      setLoading(!loading);
      await fetch(`${apiUrl}/Users/skills/${userId}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user.skills),
      }).then((data) => console.log(data));
    }
  };

  return (
    <div>
      <p>Add Skills:</p>
      <div className='inline-flex mr-96 rounded-md shadow-sm' role='group'>
        {skills &&
          skills.map((data, index) => {
            return (
              <div key={index}>
                <button
                  onClick={() => handleSkillClick(data)}
                  className='ml-1 bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow'
                >
                  {data.skillName}
                </button>
              </div>
            );
          })}
      </div>
      <p>My Skills:</p>
      <div className='inline-flex mr-96 rounded-md shadow-sm' role='group'>
        {userSkills &&
          userSkills.map((data, index) => {
            return (
              <div key={index}>
                <button className='ml-1 bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow'>
                  {data.skillName}
                </button>
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default AddSkills;
