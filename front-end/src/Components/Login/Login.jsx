import keycloak from '../../Keycloak/Keycloak';

const Login = () => {
  return (
    <div className='align-middle'>
      {keycloak.authenticated ? (
        <button
          className='p-2 bg-red-300 rounded-md mt-2'
          onClick={() => {
            keycloak.logout();
          }}
        >
          Logout
        </button>
      ) : (
        <button
          className='p-2 bg-emerald-300 rounded-md mt-2'
          onClick={() => {
            keycloak.login();
          }}
        >
          Login
        </button>
      )}
    </div>
  );
};

export default Login;
