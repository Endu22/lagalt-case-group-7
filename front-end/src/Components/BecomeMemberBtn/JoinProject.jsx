import { useState } from 'react';
import { postProjectToUser } from '../../api/projects';

//Component to use when Joining a project.
const JoinProject = ({ projectId }) => {
  const user = JSON.parse(localStorage.getItem('user'));
  const [isMember, setIsMember] = useState(false);
  let userId = [];

  const projects = JSON.parse(localStorage.getItem('projects'));
  let member = false;

  for (let i = 0; i < projects.length; i++) {
    for (let j = 0; j < projects[i].members.length; j++) {
      if (
        projects[i].members[j] === user.userId &&
        projectId === projects[i].projectId
      ) {
        member = true;
      }
    }
  }

  let id = projectId.toString();
  userId.push(user.userId);

  const handleJoinBtnClick = async () => {
    //post the project to the user function.
    await postProjectToUser(id, userId);
    setIsMember(!isMember);
  };

  return (
    <>
      {isMember || member ? (
        <div className='p-2 mt-2'></div>
      ) : (
        <button
          onClick={handleJoinBtnClick}
          className={`p-2  bg-emerald-600 rounded-lg font-semibold hover:bg-emerald-400 `}
        >
          Become member
        </button>
      )}
    </>
  );
};

export default JoinProject;
