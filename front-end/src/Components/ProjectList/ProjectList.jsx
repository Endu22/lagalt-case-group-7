import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getSortedHistory } from "../../api/projects";
import { ContextData } from "../../Context/ProjectsContext";
import keycloak from "../../Keycloak/Keycloak";
import Fields from "./Fields";

const ProjectList = () => {
    const [sortedProjects, setSortedProjects] = useState([]);
    const [error, setError] = useState("");
    //get projects and fields in localstorage.
    const user = JSON.parse(localStorage.getItem("user"));
    const projects = JSON.parse(localStorage.getItem("projects"));
    const fields = JSON.parse(localStorage.getItem("fields"));

    const getSortedData = async () => {
        if (keycloak.authenticated) {
            let [data, errorMessage] = await getSortedHistory(user.userId);
            setSortedProjects(data);
            setError(errorMessage);
        }
    };

    useEffect(() => {
       getSortedData();
    }, []);

    return (
        <div>
            <p className="text-5xl mb-4 font-bold">Projects</p>
            <div >
                {keycloak.authenticated ? <div className="grid xl:grid-cols-3 gap-5 sm:grid-cols-2 px-3 ">
                    {sortedProjects && sortedProjects.map((sorted,i) => {
                        return (
                            <Link
                            key={i}
                            to={`/projects/${sorted.projectId}`}
                            >
                                <div className="bg-slate-400 hover:shadow-xl p-5 hover:cursor-pointer">
                                    <p>{sorted.projectName}</p>
                                    <hr />
                                    <p>{sorted.description}</p>
                                    <hr />
                                    <div className="text-xs flex justify-end underline">
                                        <p>Members: </p>
                                        <p> {sorted.members.length}</p>
                                    </div>
                                    <div>
                                        <Fields
                                            projects={sorted}
                                            index={i}
                                            fields={fields}
                                            />
                                    </div>
                                </div>
                            </Link>
                        )
                    })}
                </div> :<div className="grid xl:grid-cols-3 gap-5 sm:grid-cols-2 px-3 "> {projects &&
                    projects.map((data, index) => {
                        return (
                            <Link
                            key={index}
                            to={`/projects/${data.projectId}`}
                            >
                                <div className="bg-slate-400 hover:shadow-xl p-5 hover:cursor-pointer">
                                    <p>{data.projectName}</p>
                                    <hr />
                                    <p>{data.description}</p>
                                    <hr />
                                    <div className="text-xs flex justify-end underline">
                                        <p>Members: </p>
                                        <p> {data.members.length}</p>
                                    </div>
                                    <div>
                                        <Fields
                                            projects={data}
                                            index={index}
                                            fields={fields}
                                            />
                                    </div>
                                </div>
                            </Link>
                        );
                         })}</div>}
                   
            </div>
        </div>
    );
};

export default ProjectList;
