import "../../App.css";

const Fields = ({ projects, index, fields }) => {
    return (
        <div>
            {fields.map((field, i) => {
                return (
                    <div
                        key={i}
                        className={`${field.name}  mt-2 w-36 rounded-full font-mono `}
                    >
                        {field.fieldId === projects.fieldId ? (
                            <p>{field.name}</p>
                        ) : null}
                    </div>
                );
            })}
        </div>
    );
};
export default Fields;
