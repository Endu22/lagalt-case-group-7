import { createHeadersWithToken } from "../../api/headers";
import keycloak from "../../Keycloak/Keycloak";
const apiUrl = process.env.REACT_APP_SERVER_ENDPOINT;
//This function adds new users to our database
const GetUsers = async () => {
    let users;
    let userExist = false;
    let res = await fetch(`${apiUrl}/users`, {
        headers: createHeadersWithToken(),
    });
    users = await res.json();
    //Create a user object from keycloak data.
    const user = {
        username: keycloak.tokenParsed.preferred_username,
        lastname: keycloak.tokenParsed.family_name,
        firstname: keycloak.tokenParsed.given_name,
    };
    //Check if the user already exists.
    for (let i = 0; i < users.length; i++) {
        if (users[i].username === keycloak.tokenParsed.preferred_username) {
            return (userExist = true);
        }
    }
    //if user doesn't exist, post the new user to database.
    if (!userExist) {
        await fetch(`${apiUrl}/users`, {
            method: "POST",
            headers: createHeadersWithToken(),
            body: JSON.stringify(user),
        });
        console.log("Posting user to DB");
    }
    console.log(userExist);
};
//Function that later gets called in app.js
export const CreateUserAccount = () => {
    GetUsers();
};
