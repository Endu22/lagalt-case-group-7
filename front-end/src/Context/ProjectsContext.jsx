import React, { useContext, useState, useEffect } from "react";
import keycloak from "../Keycloak/Keycloak";
const apiUrl = process.env.REACT_APP_SERVER_ENDPOINT;
const ProjectContext = React.createContext();

//Return the useContext.
export function ContextData() {
    return useContext(ProjectContext);
}

//Exported function that fetches data.
export const updateStorage = async () => {
    //Get all projcets.
    fetch(`${apiUrl}/projects`)
        .then((res) => res.json())
        .then((data) => {
            //Put all the fetched projects in localstorage.
            localStorage.setItem("projects", JSON.stringify(data));
        });

    try {
        //Fetch all users.
        fetch(`${apiUrl}/users`,{
            method:"GET",
            headers: {
                "Authorization" : `bearer ` + keycloak.token
            }
        })
            .then((res) => res.json())
            .then((data) => {
                localStorage.setItem("users", JSON.stringify(data));
                //Find the authenticated user
                for (let i = 0; i < data.length; i++) {
                    if (
                        keycloak.tokenParsed.preferred_username ===
                        data[i].username
                    ) {
                        //Place the autheticated user in localstorage.
                        localStorage.setItem("user", JSON.stringify(data[i]));
                    }
                }
            });
    } catch (error) {
        console.table(error);
    }

    try {
        //Get fields and place them inside localstorage.
        fetch(`${apiUrl}/fields`)
            .then((res) => res.json())
            .then((data) => {
                localStorage.setItem("fields", JSON.stringify(data));
            });
    } catch (error) {
        console.log(error);
    }
};

//Context.
export const ProjectsContext = ({ children }) => {
    const [projects, setProjects] = useState([]);
    const [users, setUsers] = useState([]);
    const [user, setUser] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setLoading(!loading);
        //Fetch all projects.
        fetch(`${apiUrl}/projects`)
            .then((res) => res.json())
            .then((data) => {
                localStorage.setItem("projects", JSON.stringify(data));
                setProjects(data);
                setLoading(!loading);
            });

        try {
            //Get all users
            fetch(`${apiUrl}/users`)
                .then((res) => res.json())
                .then((data) => {
                    setUsers(data);
                    localStorage.setItem("users", JSON.stringify(data));

                    for (let i = 0; i < data.length; i++) {
                        if (keycloak.authenticated) {
                            if (
                                keycloak.tokenParsed.preferred_username ===
                                data[i].username
                            ) {
                                //set an item of the authenticated user in localstorage.
                                localStorage.setItem(
                                    "user",
                                    JSON.stringify(data[i])
                                );
                                setUser(data[i]);
                            }
                        }
                    }
                });
        } catch (error) {
            console.table(error);
        }

        try {
            //get all fields.
            const reqOption = {
                method: "GET",
                headers: {
                    "Authorization": `Bearer` + keycloak.token,
                },
            };
            fetch(`${apiUrl}/fields`)
                .then((res) => res.json())
                .then((data) => {
                    localStorage.setItem("fields", JSON.stringify(data));
                });
        } catch (error) {
            console.log(error);
        }
    }, []);
    //Values to render as props to our context provider.
    const value = {
        projects,
        users,
        user,
    };

    return (
        <ProjectContext.Provider value={value}>
            {children}
        </ProjectContext.Provider>
    );
};
